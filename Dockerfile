FROM maven:3.5.4-jdk-8
ARG JAR_FILE
ADD / /
EXPOSE 9090
RUN ["mvn", "clean", "package", "-DskipTests"]
ENTRYPOINT ["mvn", "spring-boot:run"]