package com.MmoCardGame.DataServer.users;

import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import com.MmoCardGame.DataServer.cards.packets.CardsPacketOpener;
import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsersService {
    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private CardsPacketOpener packetOpener;

    public void updateUser(User user){
        usersRepository.save(user);
    }

    public List<Card> openUserPacket(String login, PacketsTypes packetType){
        User user = usersRepository.findByLogin(login);
        if(user.getNumberOfCardPackets().get(packetType) > 0) {
            user.removePacket(packetType);
            List<Card> newCards = packetOpener.openPacket(packetType);
            for(Card card : newCards){
                user.addCard(card);
            }
            usersRepository.save(user);
            return newCards;
        }
        return new ArrayList<>();
    }

    public void addUser(User user){
        usersRepository.save(user);
    }

    public void updateEmailFor(String email, String login){
        usersRepository.setEmailFor(email, login);
    }

    public boolean isEmailUsed(String email){
        User user = usersRepository.findByEmail(email);
        return user != null;
    }

    public boolean isLoginUsed(String login){
        User user = usersRepository.findByLogin(login);
        return user != null;
    }

    public User getUserByEmail(String email){
        return usersRepository.findByEmail(email);
    }

    public User getUserByLogin(String login){
        return usersRepository.findByLogin(login);
    }

    public User getUserByLoginWithCards(String login){
        return usersRepository.findByLoginWithDeckCards(login);
    }

    public void updatePasswordFor(String newPassword, String login) {
        usersRepository.setPasswordFor(newPassword, login);
    }

    public void removeDeck(String login, String deckName) {
        User user = usersRepository.findByLogin(login);
        user.removeDeck(deckName);
        usersRepository.save(user);
    }
}
