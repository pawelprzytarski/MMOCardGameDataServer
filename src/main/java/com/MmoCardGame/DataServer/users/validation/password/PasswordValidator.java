package com.MmoCardGame.DataServer.users.validation.password;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator implements ConstraintValidator<ValidPassword, String>  {

    private static final Logger logger = LoggerFactory.getLogger(PasswordValidator.class);

    private static final String PASSWORD_PATTERN = "^[A-Z0-9\":<>?,./\\[\\]{}\\\\|=\\-+_)(*&^%$#@!`~]{7,35}$";

    @Override
    public void initialize(ValidPassword validLogin) {

    }

    @Override
    public boolean isValid(String password, ConstraintValidatorContext constraintValidatorContext) {
        if(password == null) {
            logger.warn("Attempt of validating null password.");
            return false;
        }
        boolean passwordValid = isPasswordValid(password);
        logger.info("Password: {} validation result: {}", password, passwordValid);
        return passwordValid;
    }

    private boolean isPasswordValid(String password){
        Pattern pattern = Pattern.compile(PASSWORD_PATTERN, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(password);
        return matcher.matches();
    }
}
