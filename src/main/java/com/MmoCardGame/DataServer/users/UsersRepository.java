package com.MmoCardGame.DataServer.users;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface UsersRepository extends JpaRepository<User, Long> {
    User findByEmail(String email);
    User findByLogin(String login);
    @Modifying
    @Query("update USERS u set u.email = ?1 where u.login = ?2")
    int setEmailFor(String email, String login);
    @Modifying
    @Query("update USERS u set u.password = ?1 where u.login = ?2")
    int setPasswordFor(String newPassword, String login);

    @Query("select u " +
            "from USERS u " +
            "left outer join fetch u.decks decks " +
            "left outer join fetch decks.cards " +
            "where u.login = ?1")
    User findByLoginWithDeckCards(String login);
}
