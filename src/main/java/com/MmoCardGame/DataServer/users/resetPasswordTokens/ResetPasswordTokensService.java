package com.MmoCardGame.DataServer.users.resetPasswordTokens;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Component
public class ResetPasswordTokensService {

    private static final Logger logger = LoggerFactory.getLogger(ResetPasswordTokensService.class);

    private Random random = new Random();

    private Map<String, Token> tokens = new ConcurrentHashMap<>();

    public String generateNewToken() {
        String code;
        do {
            int randomCode = random.nextInt(1000000);
            code = String.format("%06d", randomCode);
        } while(tokens.containsKey(code));
        return code;
    }

    @Scheduled(fixedDelay = TokensSettings.TOKENS_EXPIRATION_VALIDATION_DELAY_MILLISECONDS)
    public void validateTokens() {
        logger.info("Time of token validation: " + new Date());
        for(Token token: tokens.values()){
            if(isTokenExpired(token)){
                System.out.println("Token expired: " + token);
                remove(token.getToken());
                logger.warn("Token for resetting password expired. Token: {}, for user: {}", token.getToken(), token.getUserLogin());
            }
        }
    }

    private boolean isTokenExpired(Token token) {
        Set<String> tokenErrorMessages = tokenErrorMessages(token);
        return tokenErrorMessages.contains(TokensSettings.TOKEN_EXPIRED_MESSAGE);
    }

    private boolean isTokenValid(Token token){
        Set<String> tokenErrorMessages = tokenErrorMessages(token);
        return !tokenErrorMessages.contains(TokensSettings.INVALID_TOKEN_MESSAGE);
    }

    private Set<String> tokenErrorMessages(Token token){
        Set<ConstraintViolation<Token>> violations = validToken(token);
        return violations.stream().map(ConstraintViolation::getMessage).collect(Collectors.toSet());
    }

    private Set<ConstraintViolation<Token>> validToken(Token token) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        return validator.validate(token);
    }

    public void store(String token, String userLogin) {
        Token newToken = new Token(token, new Date(), userLogin);
        if(isTokenValid(newToken)) {
            tokens.put(token, newToken);
        }
    }

    public boolean contains(String token) {
        return tokens.get(token) != null;
    }

    public String retrieveLogin(String token) {
        return tokens.get(token).getUserLogin();
    }

    public void remove(String token){
        tokens.remove(token);
    }

    public void removeAll(){
        tokens.clear();
    }

    public int size(){
        return tokens.size();
    }

    public Collection<String> getTokens(){
        return tokens.keySet();
    }
}
