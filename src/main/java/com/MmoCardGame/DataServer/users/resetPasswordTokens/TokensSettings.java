package com.MmoCardGame.DataServer.users.resetPasswordTokens;

public class TokensSettings {
    public static final int TOKEN_EXPIRATION_DAYS = 5;
    public static final long TOKENS_EXPIRATION_VALIDATION_DELAY_HOURS = 12;
    public static final long TOKENS_EXPIRATION_VALIDATION_DELAY_MILLISECONDS = TOKENS_EXPIRATION_VALIDATION_DELAY_HOURS * 60 * 60 * 1000;

    public static final String TOKEN_EXPIRED_MESSAGE = "Token expired.";
    public static final String INVALID_TOKEN_MESSAGE = "Token is invalid.";
}
