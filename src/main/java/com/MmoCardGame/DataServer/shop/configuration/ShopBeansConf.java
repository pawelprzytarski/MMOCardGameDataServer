package com.MmoCardGame.DataServer.shop.configuration;

import com.MmoCardGame.DataServer.shop.ShopService;
import com.MmoCardGame.DataServer.shop.productsFileManagement.ProductsFromFileLoader;
import com.MmoCardGame.DataServer.shop.productsFileManagement.ProductsLoader;
import com.MmoCardGame.DataServer.shop.productsFileManagement.ProductsSaver;
import com.MmoCardGame.DataServer.shop.productsFileManagement.ProductsToFileSaver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ShopBeansConf {

    @Bean
    public ShopService shop(ProductsLoader productsLoader){
        return new ShopService(productsLoader);
    }

    @Bean(name = "productsFilePath")
    public String productsFilePath(){
        return ShopSettings.PRODUCTS_FILE_PATH;
    }

//method for programmatically changing shop products, other option is editing json
//    @Bean
//    public CommandLineRunner initProducts(CardsService cardsService) {
//        return(evt) -> {
//            List<Product> products = new ArrayList()
//            products.add(new Product(...));
//            productsSaver().save(products);
//        };
//    }

    @Bean
    public ProductsSaver productsSaver(){
        return new ProductsToFileSaver(productsFilePath());
    }

    @Bean
    public ProductsLoader productsLoader(){
        return new ProductsFromFileLoader(productsFilePath());
    }

}
