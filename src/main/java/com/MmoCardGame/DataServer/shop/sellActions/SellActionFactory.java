package com.MmoCardGame.DataServer.shop.sellActions;

import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import com.MmoCardGame.DataServer.exceptions.ProductWithThatNameDoesNotExist;
import com.MmoCardGame.DataServer.shop.products.ProductNames;

public class SellActionFactory {

    public static ProductSellAction produce(ProductNames name){
        if(name.equals(ProductNames.MEDIUM_PACKET)){
            return new PacketSellAction(PacketsTypes.MEDIUM_PACKET);
        } else if(name.equals(ProductNames.BIG_PACKET)){
            return new PacketSellAction(PacketsTypes.BIG_PACKET);
        } else if(name.equals(ProductNames.SMALL_PACKET)){
            return new PacketSellAction(PacketsTypes.SMALL_PACKET);
        }

        throw new ProductWithThatNameDoesNotExist(name);
    }

}
