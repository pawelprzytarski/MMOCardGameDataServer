package com.MmoCardGame.DataServer.shop.sellActions;

import com.MmoCardGame.DataServer.shop.products.Product;
import com.MmoCardGame.DataServer.users.User;

public interface ProductSellAction {
    void perform(Product product, User user);
}
