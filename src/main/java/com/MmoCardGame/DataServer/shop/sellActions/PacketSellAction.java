package com.MmoCardGame.DataServer.shop.sellActions;

import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import com.MmoCardGame.DataServer.shop.products.Product;
import com.MmoCardGame.DataServer.users.User;
import lombok.Data;

@Data
public class PacketSellAction implements ProductSellAction {

    private PacketsTypes type;

    public PacketSellAction(PacketsTypes type){
        this.type = type;
    }

    @Override
    public void perform(Product product, User user) {
        user.addPacket(type);
        user.decreaseMoney(product.getPrice());
    }
}
