package com.MmoCardGame.DataServer.shop.productsFileManagement;

import com.MmoCardGame.DataServer.shop.products.Product;

import java.util.List;

public interface ProductsLoader {
    List<Product> load();
}
