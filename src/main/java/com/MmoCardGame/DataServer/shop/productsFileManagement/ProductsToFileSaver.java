package com.MmoCardGame.DataServer.shop.productsFileManagement;

import com.MmoCardGame.DataServer.shop.products.Product;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class ProductsToFileSaver implements ProductsSaver {
    private static final Logger logger = LoggerFactory.getLogger(ProductsToFileSaver.class);

    private static final ObjectMapper objectMapper = new ObjectMapper();
    private String filePath;

    @Autowired
    public  ProductsToFileSaver(@Qualifier("productsFilePath") String filePath){
        this.filePath = filePath;
    }

    @Override
    public void save(List<Product> products) {
        try(PrintWriter writer = new PrintWriter(filePath))
        {
            String productsJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(products);
            writer.print(productsJson);
            logProducts(products);
        } catch (FileNotFoundException | JsonProcessingException e) {
            logger.debug(e.getMessage(), e);
        }
    }

    private void logProducts(List<Product> products) {
        logger.info("Products saved to file.");
        logger.info("File path: {}", filePath);
        logger.info("Products:");
        for(Product product: products){
            logger.info("Product name: {}, category: {}, price: {}", product.getName(), product.getCategory(), product.getPrice());
        }
    }

}
