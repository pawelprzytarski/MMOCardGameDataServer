package com.MmoCardGame.DataServer.shop;

import com.MmoCardGame.DataServer.REST.responses.ResponseMessages;
import com.MmoCardGame.DataServer.shop.products.Product;
import com.MmoCardGame.DataServer.shop.products.ProductCategories;
import com.MmoCardGame.DataServer.shop.products.ProductNames;
import com.MmoCardGame.DataServer.shop.productsFileManagement.ProductsLoader;
import com.MmoCardGame.DataServer.shop.sellActions.ProductSellAction;
import com.MmoCardGame.DataServer.shop.sellActions.SellActionFactory;
import com.MmoCardGame.DataServer.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class ShopService {

    private static final Logger logger = LoggerFactory.getLogger(ShopService.class);

    private Map<ProductNames, Product> products;

    @Autowired
    public ShopService(ProductsLoader productsLoader){
        products = new HashMap<>();
        List<Product> productsList = productsLoader.load();
        productsList.forEach(product -> products.put(product.getName(), product));
    }

    public List<ProductCategories> getProductsCategories(){
        return Arrays.asList(ProductCategories.values());
    }

    public String tryBuyProduct(User user, ProductNames name){
        Product productToBuy = products.get(name);
        if(user.getMoney() >= productToBuy.getPrice()){
            ProductSellAction sellAction = SellActionFactory.produce(productToBuy.getName());
            sellAction.perform(productToBuy, user);
            logger.info("Product bought by user. Name: {}, Price: {}", productToBuy.getName(), productToBuy.getPrice());
            return ResponseMessages.PRODUCT_BOUGHT;
        }
        logger.info("Product too expensive for user. Name: {}, Price: {}, user money: {}", productToBuy.getName(), productToBuy.getPrice(), user.getMoney());
        return ResponseMessages.PRODUCT_TO_EXPENSIVE;
    }

    public Product getProduct(ProductNames name){
        return products.get(name);
    }

    public int getProductPrice(ProductNames name){
        return products.get(name).getPrice();
    }

    public List<Product> getProductsOfCategory(ProductCategories category){
        List<Product> result = new ArrayList<>();
        for(Product product:products.values()){
            if(product.getCategory().equals(category)){
                result.add(product);
            }
        }
        return result;
    }

    public List<Product> getProducts() {
        return new ArrayList<>(products.values());
    }
}
