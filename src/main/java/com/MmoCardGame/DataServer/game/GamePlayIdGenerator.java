package com.MmoCardGame.DataServer.game;

import java.util.UUID;

public class GamePlayIdGenerator {

    public static String generateId(){
        return UUID.randomUUID().toString();
    }

}
