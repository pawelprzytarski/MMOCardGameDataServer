package com.MmoCardGame.DataServer.game.configuration;

public class GameSettings {
    public static final int TIME_BETWEEN_GAME_PLAYS_CREATION = 500;
    public static final int WINNER_AWARD = 100;
    public static final int LOOSER_AWARD = 50;
}
