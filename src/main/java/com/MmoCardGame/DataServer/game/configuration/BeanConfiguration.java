package com.MmoCardGame.DataServer.game.configuration;

import com.MmoCardGame.DataServer.appConfiguration.AppProperties;
import com.MmoCardGame.DataServer.game.battleServerClient.BattleServerClient;
import com.MmoCardGame.DataServer.game.battleServerClient.BattleServerClientImpl;
import com.MmoCardGame.DataServer.game.battleServerClient.BattleServerClientMock;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanConfiguration {
    private static final Logger logger = LoggerFactory.getLogger(BeanConfiguration.class);

    private AppProperties appProperties;

    @Autowired
    public BeanConfiguration(AppProperties appProperties){
        this.appProperties = appProperties;
    }

    @Bean
    public BattleServerClient battleServerClient(){
        if(appProperties.getGeneralAppProperties().getUseMockedBattleServer()){
            logger.info("Creating mocked battle server.");
            return new BattleServerClientMock();
        } else {
            logger.info("Creating battle server.");
            return new BattleServerClientImpl();
        }
    }

}
