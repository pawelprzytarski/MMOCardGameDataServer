package com.MmoCardGame.DataServer.game.battleServerClient;

import com.MMOCardGame.BattleServer.Servers.ProtoBuffers.InnerServerGrpc;
import com.MMOCardGame.BattleServer.Servers.ProtoBuffers.NewSessionData;
import com.MMOCardGame.BattleServer.Servers.ProtoBuffers.Reply;
import com.MMOCardGame.BattleServer.Servers.ProtoBuffers.UserData;
import com.MmoCardGame.DataServer.appConfiguration.AppProperties;
import com.MmoCardGame.DataServer.game.data.GamePlay;
import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import com.MmoCardGame.DataServer.users.User;
import io.grpc.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
public class BattleServerClientImpl implements BattleServerClient {
    private static final Logger logger = LoggerFactory.getLogger(BattleServerClientImpl.class);

    private ManagedChannel channel;
    private InnerServerGrpc.InnerServerBlockingStub blockingStub;
    private InnerServerGrpc.InnerServerStub asyncStub;
    public static final String SEPARATOR = "========================================================";

    @Autowired
    private AppProperties appProperties;

    @PostConstruct
    public void init() {
        init(ManagedChannelBuilder.forAddress(
                appProperties.getGeneralAppProperties().getBattleServerUrl(),
                appProperties.getGeneralAppProperties().getBattleServerPort())
                .usePlaintext());
        logConfiguration();
    }

    private void logConfiguration() {
        logger.info(SEPARATOR);
        logger.info("Battle server initialized with:");
        logger.info("Url  : {}", appProperties.getGeneralAppProperties().getBattleServerUrl());
        logger.info("Port : {}", appProperties.getGeneralAppProperties().getBattleServerPort());
        logger.info("Token: {}", appProperties.getGeneralAppProperties().getBattleServerToken());
        logger.info(SEPARATOR);
    }

    public void init(ManagedChannelBuilder<?> channelBuilder) {
        channel = channelBuilder.build();
        blockingStub = getServerBlockingStubWithAddingHeader(
                channel,
                appProperties.getGeneralAppProperties().getBattleServerToken());
        asyncStub = InnerServerGrpc.newStub(channel);
    }

    private InnerServerGrpc.InnerServerBlockingStub getServerBlockingStubWithAddingHeader(Channel channel, String token) {
        return InnerServerGrpc.newBlockingStub(channel)
                .withInterceptors(new ClientInterceptor() {
                    @Override
                    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> methodDescriptor, CallOptions callOptions, Channel channel1) {
                        return new ForwardingClientCall.SimpleForwardingClientCall<ReqT, RespT>(channel1.newCall(methodDescriptor, callOptions)) {
                            @Override
                            public void start(Listener<RespT> responseListener, Metadata headers) {
                                headers.put(Metadata.Key.of("Token", Metadata.ASCII_STRING_MARSHALLER), token);
                                super.start(responseListener, headers);
                            }
                        };
                    }
                });
    }

    public void sendGamePlayInfoToBattleServer(GamePlay gamePlay){
        int gameType = GamePlayTypes.getSingleOrTeamType(gamePlay.getGameType());

        List<UserData> usersData = buildUserData(gamePlay);

        NewSessionData sessionData = buildSessionData(gamePlay, gameType, usersData);

        logSessionRegisterRequest(gamePlay, gameType, usersData);

        Reply reply = blockingStub.registerGameSession(sessionData);

        logger.info("Received session start request reply: {}", reply);
    }

    private NewSessionData buildSessionData(GamePlay gamePlay, int gameType, List<UserData> usersData) {
        return NewSessionData.newBuilder()
                    .addAllUsers(usersData)
                    .setGameType(gameType)
                    .setSessionId(gamePlay.getSessionId()).build();
    }

    private List<UserData> buildUserData(GamePlay gamePlay) {
        List<UserData> usersData = new ArrayList<>();
        gamePlay.getPlayers().values().forEach(player -> {
            List<Integer> cardsIds = getCardsIds(player);
            UserData userData = UserData.newBuilder()
                    .setToken(gamePlay.getAuthTokens().get(player.getLogin()))
                    .setUsername(player.getLogin())
                    .addAllCardIds(cardsIds)
                    .setHeroId(1)
                    .build();
            usersData.add(userData);
        });
        return usersData;
    }

    private void logSessionRegisterRequest(GamePlay gamePlay, int gameType, List<UserData> usersData) {
        logger.info(SEPARATOR);
        logger.info("Sent registering game session data:");
        logger.info("Game type  : {} = {}", gameType, gamePlay.getGameType());
        logger.info("session id : {}", gamePlay.getSessionId());
        logger.info("Users data:");
        for(UserData userData : usersData) {
            logger.info("token: {} \n cards amount: {}",
                    userData.getToken(), userData.getCardIdsCount());
        }
        logger.info(SEPARATOR);
    }

    private List<Integer> getCardsIds(User player) {
        List<Integer> cardsIds = new ArrayList<>();
        player.getSelectedDeck().getCards().forEach(deckCard -> {
            int cardId = deckCard.getCard().getId().intValue();
            for(int i = 0; i < deckCard.getAmount(); i++){
                cardsIds.add(cardId);
            }
        });
        return cardsIds;
    }
}