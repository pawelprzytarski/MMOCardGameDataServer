package com.MmoCardGame.DataServer.game.battleServerClient;

import com.MmoCardGame.DataServer.game.data.GamePlay;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BattleServerClientMock implements BattleServerClient{
    private static final Logger logger = LoggerFactory.getLogger(BattleServerClientMock.class.getName());

    public void sendGamePlayInfoToBattleServer(GamePlay gamePlay){
        logger.info("Mocked sending data to battle server.");
    }

}
