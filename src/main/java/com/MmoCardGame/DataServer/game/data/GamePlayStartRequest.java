package com.MmoCardGame.DataServer.game.data;

import com.MmoCardGame.DataServer.users.User;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GamePlayStartRequest {

    private User user;

    private GamePlayTypes gameType;

}
