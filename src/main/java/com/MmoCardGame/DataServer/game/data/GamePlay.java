package com.MmoCardGame.DataServer.game.data;

import com.MmoCardGame.DataServer.game.AuthTokensGenerator;
import com.MmoCardGame.DataServer.game.GamePlayIdGenerator;
import com.MmoCardGame.DataServer.game.SessionIdGenerator;
import com.MmoCardGame.DataServer.users.User;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;
import java.util.Set;

@Data
public class GamePlay {

    private static final Logger logger = LoggerFactory.getLogger(GamePlay.class);

    private String gameId;
    private GamePlayTypes gameType;
    private Map<String, String> authTokens;
    private Map<String, User> players;
    private Set<String> playersStillInGame;
    private Date startTime;
    private GamePlayStates state;
    private int sessionId;

    public GamePlay(GamePlayTypes gameType, Map<String, User> players, Date startTime) {
        this.gameType = gameType;
        this.players = players;
        this.authTokens = AuthTokensGenerator.generateMap(players.keySet());
        this.playersStillInGame = players.keySet();
        this.startTime = startTime;
        this.gameId = GamePlayIdGenerator.generateId();
        this.state = GamePlayStates.INITIALIZED;
        start();
    }

    public boolean hasSession(){
        return state.equals(GamePlayStates.LASTING);
    }

    public int getSessionId(){
        return sessionId;
    }

    public void endGamePlayByPlayer(String login){
        playersStillInGame.remove(login);
        logger.info("Player {} end game.", login);
        if(playersStillInGame.isEmpty()){
            end();
        }
    }

    private void end(){
        if(state.equals(GamePlayStates.LASTING)) {
            this.state = GamePlayStates.ENDED;
            logger.info("End of the game play, with session id: {}", sessionId);
        }
    }

    private void start(){
        if(!state.equals(GamePlayStates.LASTING)) {
            this.state = GamePlayStates.LASTING;
            this.sessionId = SessionIdGenerator.getId();
            logger.info("Start game play, with new session id: {}", sessionId);
        }
    }
}
