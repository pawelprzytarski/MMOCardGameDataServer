package com.MmoCardGame.DataServer.game.data;

public enum GamePlayStates {
    INITIALIZED, LASTING, ENDED, SUSPENDED
}
