package com.MmoCardGame.DataServer.game.playersQueues;

import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import org.springframework.stereotype.Service;

@Service
public class ThreePlayersQueue extends PlayersQueueImpl {
    public ThreePlayersQueue() {
        super(3, GamePlayTypes.THREE_PLAYERS);
    }
}
