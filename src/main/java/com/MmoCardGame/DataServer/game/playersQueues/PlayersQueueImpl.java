package com.MmoCardGame.DataServer.game.playersQueues;

import com.MmoCardGame.DataServer.exceptions.NotEnoughPlayersInQueueToStartGamePlayException;
import com.MmoCardGame.DataServer.game.data.GamePlay;
import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import com.MmoCardGame.DataServer.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;

public abstract class PlayersQueueImpl implements PlayersQueue {
    private static final Logger logger = LoggerFactory.getLogger(PlayersQueueImpl.class);

    protected Queue<User> queue = new ConcurrentLinkedQueue<>();

    private int numberOfPlayers;
    private  GamePlayTypes gamePlayType;

    public PlayersQueueImpl(int numberOfPlayers, GamePlayTypes gamePlayType){
        this.numberOfPlayers = numberOfPlayers;
        this.gamePlayType = gamePlayType;
        logger.info("Created players queue for game play type: {}", gamePlayType);
    }

    @Override
    public GamePlay createGame() {
        if(!canCreateGame()) {
            throw new NotEnoughPlayersInQueueToStartGamePlayException(
                                "Min players to start game play is " + numberOfPlayers + ", was " + queue.size()
                        );
        }

        Map<String, User> players = new HashMap<>();
        for(int i = 0; i < numberOfPlayers; i++){
            User player = queue.remove();
            players.put(player.getLogin(), player);
        }
        GamePlay newGame = new GamePlay(gamePlayType, players, new Date());
        logger.info("Created game play.");
        logger.info("Game play type: {}", gamePlayType);
        logger.info("Game play users:");
        for(User player:newGame.getPlayers().values()){
            logger.info("Login: {}", player.getLogin());
        }
        return newGame;
    }

    @Override
    public boolean canCreateGame() {
        return queue.size() >= numberOfPlayers;
    }

    @Override
    public void add(User user) {
        queue.add(user);
    }

    @Override
    public void remove(User user) {
        queue.remove(user);
    }

    @Override
    public boolean contains(User user){
        return queue.contains(user);
    }

    @Override
    public int size(){
        return queue.size();
    }

    @Override
    public void clear(){
        queue.clear();
    }
}
