package com.MmoCardGame.DataServer.game.playersQueues;

import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import org.springframework.stereotype.Service;

@Service
public class FourPlayersQueue extends  PlayersQueueImpl {
    public FourPlayersQueue() {
        super(4, GamePlayTypes.FOUR_PLAYERS);
    }
}
