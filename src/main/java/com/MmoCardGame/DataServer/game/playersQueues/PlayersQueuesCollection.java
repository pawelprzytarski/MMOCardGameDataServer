package com.MmoCardGame.DataServer.game.playersQueues;

import com.MmoCardGame.DataServer.exceptions.QueueForThisGameTypeNotSupported;
import com.MmoCardGame.DataServer.exceptions.UserIsAlreadyWaitingForGamePlay;
import com.MmoCardGame.DataServer.game.data.GamePlay;
import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import com.MmoCardGame.DataServer.users.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PlayersQueuesCollection {

    public static final Logger logger = LoggerFactory.getLogger(PlayersQueuesCollection.class);

    private TwoPlayersQueue twoPlayersQueue;
    private ThreePlayersQueue threePlayersQueue;
    private FourPlayersQueue fourPlayersQueue;
    private TeamPlayersQueue teamPlayersQueue;

    @Autowired
    public PlayersQueuesCollection(TwoPlayersQueue twoPlayersQueue,
                                   ThreePlayersQueue threePlayersQueue,
                                   FourPlayersQueue fourPlayersQueue,
                                   TeamPlayersQueue teamPlayersQueue){
        this.twoPlayersQueue = twoPlayersQueue;
        this.threePlayersQueue = threePlayersQueue;
        this.fourPlayersQueue = fourPlayersQueue;
        this.teamPlayersQueue = teamPlayersQueue;
    }

    public boolean isUserWaiting(User user){
        for(GamePlayTypes gamePlayType : GamePlayTypes.values()){
            if(getQueue(gamePlayType).contains(user)){
                return true;
            }
        }
        return false;
    }

    public void removeWaitingPlayer(User user){
        for(GamePlayTypes gamePlayType : GamePlayTypes.values()){
            if(getQueue(gamePlayType).contains(user)){
                getQueue(gamePlayType).remove(user);
                logger.info("Removing player {} from queue: {}", user.getLogin(), gamePlayType);
                return;
            }
        }
    }

    public GamePlay createGame(GamePlayTypes gameType){
        return getQueue(gameType).createGame();
    }

    public boolean canCreateGame(GamePlayTypes gameType){
        return getQueue(gameType).canCreateGame();
    }

    public void putToQueue(GamePlayTypes gameType, User user) {
        if(!isUserWaiting(user)) {
            getQueue(gameType).add(user);
            logger.info("Adding player {} to queue: {}", user.getLogin(), gameType);
        } else{
            throw new UserIsAlreadyWaitingForGamePlay(user.getLogin());
        }
    }

    public void clearAllQueues(){
        for(GamePlayTypes type: GamePlayTypes.values()){
            getQueue(type).clear();
        }
    }

    public PlayersQueue getQueue(GamePlayTypes gameType){
        if(gameType.equals(GamePlayTypes.TWO_PLAYERS)){
            return twoPlayersQueue;
        } else if(gameType.equals(GamePlayTypes.THREE_PLAYERS)){
            return threePlayersQueue;
        } else if(gameType.equals(GamePlayTypes.FOUR_PLAYERS)){
            return fourPlayersQueue;
        } else if(gameType.equals(GamePlayTypes.TEAM_GAME)){
            return teamPlayersQueue;
        }

        throw new QueueForThisGameTypeNotSupported(gameType);
    }

}
