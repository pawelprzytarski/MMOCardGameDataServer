package com.MmoCardGame.DataServer.game.playersQueues;

import com.MmoCardGame.DataServer.game.data.GamePlay;
import com.MmoCardGame.DataServer.users.User;

public interface PlayersQueue {
    void add(User user);
    GamePlay createGame();
    void remove(User user);
    boolean canCreateGame();
    boolean contains(User user);
    int size();
    void clear();
}
