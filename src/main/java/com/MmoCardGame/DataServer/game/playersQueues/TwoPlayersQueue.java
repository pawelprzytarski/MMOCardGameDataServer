package com.MmoCardGame.DataServer.game.playersQueues;

import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import org.springframework.stereotype.Service;

@Service
public class TwoPlayersQueue extends PlayersQueueImpl {
    public TwoPlayersQueue() {
        super(2, GamePlayTypes.TWO_PLAYERS);
    }
}
