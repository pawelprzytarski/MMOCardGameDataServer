package com.MmoCardGame.DataServer.game;

import com.MmoCardGame.DataServer.game.data.GamePlay;

import java.util.HashMap;
import java.util.Map;

public class GamePlaysTokensService {

    private Map<String, GamePlay> gamePlays = new HashMap<>();

    public void store(GamePlay gamePlay){
        gamePlays.put(gamePlay.getGameId(),gamePlay);
    }

    public void remove(GamePlay gamePlay){
        gamePlays.remove(gamePlay.getGameId());
    }

}
