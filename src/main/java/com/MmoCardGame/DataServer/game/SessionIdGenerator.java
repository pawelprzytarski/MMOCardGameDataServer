package com.MmoCardGame.DataServer.game;

public class SessionIdGenerator {
    private static int id = 1;

    public synchronized static int getId(){
        return id++;
    }
}
