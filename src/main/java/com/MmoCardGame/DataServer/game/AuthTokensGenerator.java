package com.MmoCardGame.DataServer.game;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

public class AuthTokensGenerator {
    public static String generate(){
        return UUID.randomUUID().toString();
    }

    public static Map<String, String> generateMap(Set<String> logins){
        Map<String, String> result = new HashMap<>();
        logins.forEach(login -> result.put(login, generate()));
        return result;
    }
}
