package com.MmoCardGame.DataServer.game.results;

import com.MmoCardGame.DataServer.game.data.GamePlayResults;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GamePlayResult {
    private String login;
    private int award;
    private GamePlayResults result;
}
