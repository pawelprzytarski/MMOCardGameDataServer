package com.MmoCardGame.DataServer.game.results;

import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class LastGamePlaysResultsService {
    private Map<String, GamePlayResult> results = new HashMap<>();

    public void addResult(GamePlayResult result){
        results.put(result.getLogin(), result);
    }

    public GamePlayResult getResult(String login){
        return results.get(login);
    }

    public boolean hasResult(String login){
        return results.containsKey(login);
    }

    public void clear(){
        results.clear();
    }
}
