package com.MmoCardGame.DataServer.game;

import com.MmoCardGame.DataServer.game.data.GamePlay;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class GamePlaysCollection {

    private Map<String, GamePlay> existingGamePlays = new ConcurrentHashMap<>();
    private Map<String, GamePlay> usersGames = new ConcurrentHashMap<>();
    private Map<Integer, String> sessionIds = new ConcurrentHashMap<>();

    public GamePlaysCollection(){

    }

    public void addGamePlay(GamePlay gamePlay){
        existingGamePlays.put(gamePlay.getGameId(), gamePlay);
        gamePlay.getPlayers().keySet().forEach(login -> usersGames.put(login, gamePlay));
        sessionIds.put(gamePlay.getSessionId(), gamePlay.getGameId());
    }

    public boolean userHasGame(String login){
        return usersGames.containsKey(login);
    }

    public GamePlay getGamePlayForUser(String login){
        return usersGames.get(login);
    }

    public void removePlayerFromGamePlay(String login){
        usersGames.remove(login);
    }

    public Map<String, GamePlay> getGamePlays() {
        return existingGamePlays;
    }

    public boolean sessionExist(int sessionId) {
        return sessionIds.containsKey(sessionId);
    }

    public void removeGamePlay(String gamePlayId){
        GamePlay gamePlay = existingGamePlays.get(gamePlayId);
        gamePlay.getPlayersStillInGame().forEach(login -> usersGames.remove(login));
        existingGamePlays.remove(gamePlayId);
        sessionIds.remove(gamePlay.getSessionId());
    }

    public void removeAllGamePlays(){
        existingGamePlays.keySet().forEach(this::removeGamePlay);
    }

    public GamePlay getGamePlay(int sessionId) {
        return existingGamePlays.get(sessionIds.get(sessionId));
    }
}
