package com.MmoCardGame.DataServer.security.tokenAuthentication;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class TokenService {

    private static final Logger logger = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

    private static final Map<String, Authentication> tokens = new ConcurrentHashMap<>();

    public String generateNewToken() {
        return UUID.randomUUID().toString();
    }

    public void logoutAllUsers(){
        tokens.keySet().forEach(this::logoutUserByToken);
    }

    public void logoutUserByToken(String token){
        retrieve(token).setAuthenticated(false);
        remove(token);
        logger.info("Successfully logged out. Token: {}", token);
    }

    public void store(String token, Authentication authentication) {
        tokens.put(token, authentication);
    }

    public boolean contains(String token) {
        return tokens.containsKey(token);
    }

    public Authentication retrieve(String token) {
        return tokens.get(token);
    }

    public void remove(String token){
        tokens.remove(token);
    }

    public void removeAll(){
        tokens.clear();
    }

    public Collection<String> getTokens(){
        return tokens.keySet();
    }
}
