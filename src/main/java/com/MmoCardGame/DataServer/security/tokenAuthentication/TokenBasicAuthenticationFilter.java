package com.MmoCardGame.DataServer.security.tokenAuthentication;

import com.MmoCardGame.DataServer.REST.utilConstants.HeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class TokenBasicAuthenticationFilter extends BasicAuthenticationFilter {

    private static final Logger log = LoggerFactory.getLogger(TokenBasicAuthenticationFilter.class);

    private TokenService tokenService;

    @Autowired
    public TokenBasicAuthenticationFilter(AuthenticationManager authenticationManager, TokenService tokenService) {
        super(authenticationManager);
        this.tokenService = tokenService;
    }

    @Override
    protected void onSuccessfulAuthentication(final HttpServletRequest request,
                                              final HttpServletResponse response,
                                              final Authentication authResult) {
        String token = tokenService.generateNewToken();
        tokenService.store(token, authResult);
        response.setHeader(HeaderNames.TOKEN_HEADER_NAME, token);
        log.info("Successfully authenticated with basic authentication. Generated session token: {}", token);
    }

}