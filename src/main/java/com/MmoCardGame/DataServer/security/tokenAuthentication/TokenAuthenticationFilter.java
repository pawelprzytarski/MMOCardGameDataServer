package com.MmoCardGame.DataServer.security.tokenAuthentication;

import com.MmoCardGame.DataServer.REST.utilConstants.HeaderNames;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class TokenAuthenticationFilter extends GenericFilterBean {

    private static final Logger log = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

    private TokenService tokenService;

    @Autowired
    public TokenAuthenticationFilter(TokenService tokenService){
        this.tokenService = tokenService;
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
            throws IOException, ServletException
    {
        final HttpServletRequest httpRequest = (HttpServletRequest)request;

        final String accessToken = httpRequest.getHeader(HeaderNames.TOKEN_HEADER_NAME);
        if (accessToken != null) {
            if (tokenService.contains(accessToken)) {
                Authentication authentication = tokenService.retrieve(accessToken);
                SecurityContextHolder.getContext().setAuthentication(authentication);
                log.info("User with authentication: {} \n Successfully authenticated with token: {}", authentication, accessToken);
            } else {
                log.warn("Trying to authenticate with invalid token: {}", accessToken);
            }
        }
        chain.doFilter(request, response);
    }

}