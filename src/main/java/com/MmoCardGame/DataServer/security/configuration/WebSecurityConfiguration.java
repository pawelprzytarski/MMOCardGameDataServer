package com.MmoCardGame.DataServer.security.configuration;

import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.security.tokenAuthentication.TokenAuthenticationFilter;
import com.MmoCardGame.DataServer.security.tokenAuthentication.TokenBasicAuthenticationFilter;
import com.MmoCardGame.DataServer.security.tokenAuthentication.TokenService;
import com.MmoCardGame.DataServer.security.users.SecurityUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.http.HttpServletResponse;

@Configuration
@EnableWebSecurity
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter{

    private static final String ADMIN_ROLE = "ADMIN";

    @Autowired
    private SecurityUsersService userDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .authorizeRequests()
                    .antMatchers(ApiPaths.LOGGED_USERS_URL + "/**").permitAll()
                    .antMatchers(ApiPaths.USERS_URL + "/{login}/**").access("@userPathLoginChecker.check(authentication,#login)")
                    .antMatchers(ApiPaths.SHOP_URL).permitAll()
                    .antMatchers(ApiPaths.ADMIN_URL + "/**").hasRole(ADMIN_ROLE)
                    .antMatchers(ApiPaths.SERVER_URL + "/**").hasRole(ADMIN_ROLE)
                    .anyRequest().authenticated()
                    .and()
                .anonymous().disable()
                .exceptionHandling().authenticationEntryPoint(unauthorizedEntryPoint())
                    .and()
                .httpBasic();

        TokenService tokenService = tokenService();
        final TokenAuthenticationFilter tokenFilter = new TokenAuthenticationFilter(tokenService);
        http.addFilterBefore(tokenFilter, BasicAuthenticationFilter.class);

        final TokenBasicAuthenticationFilter customBasicAuthFilter =
                new TokenBasicAuthenticationFilter(authenticationManager(), tokenService);
        http.addFilter(customBasicAuthFilter);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
            .ignoring()
                .antMatchers(HttpMethod.POST, ApiPaths.USERS_URL) //registration url should be always accessible
                .antMatchers(HttpMethod.GET, ApiPaths.SERVER_URL + "/canConnect")
                .antMatchers(ApiPaths.USERS_URL +"/{login}/password/reset")
                .antMatchers(ApiPaths.GAME_PLAYS_URL + "/**")
                .antMatchers(ApiPaths.SWAGGER_URL)
                .antMatchers(ApiPaths.SWAGGER_UI_HTML_URL + "/**")
                .antMatchers(ApiPaths.WEBJARS_URL + "/**")
                .antMatchers(ApiPaths.SWAGGER_RESOURCES_URL + "/**");


    }

    @Bean
    public DaoAuthenticationProvider authProvider() {
        DaoAuthenticationProvider authProvider = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(passwordEncoder());
        return authProvider;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    public void configAuthBuilder(AuthenticationManagerBuilder builder) throws Exception {
        builder.authenticationProvider(authProvider());
        builder.inMemoryAuthentication().withUser("admin").password("admin").roles(ADMIN_ROLE);
    }

    @Bean
    public TokenService tokenService() {
        return new TokenService();
    }

    @Bean
    public AuthenticationEntryPoint unauthorizedEntryPoint() {
        return (request, response, authException) -> response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
    }

    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return authenticationManager();
    }
}
