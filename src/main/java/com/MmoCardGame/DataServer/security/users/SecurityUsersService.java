package com.MmoCardGame.DataServer.security.users;

import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
public class SecurityUsersService implements UserDetailsService {

    @Autowired
    private UsersService usersService;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        User user = usersService.getUserByLogin(login);
        if(user == null){
            throw new UsernameNotFoundException(login);
        }else{
            UserDetails details = new SecurityUserWrapper(user);
            return details;
        }
    }
}
