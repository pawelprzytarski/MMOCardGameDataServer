package com.MmoCardGame.DataServer.cards.deckValidation;

import com.MmoCardGame.DataServer.cards.CardTypes;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsService;
import com.MmoCardGame.DataServer.cards.cardsEntities.Deck;
import com.MmoCardGame.DataServer.cards.cardsEntities.DeckCard;
import com.MmoCardGame.DataServer.cards.configuration.CardsConsts;
import com.MmoCardGame.DataServer.cards.deckValidation.deckValidationResults.DeckValidationResult;
import com.MmoCardGame.DataServer.cards.deckValidation.deckValidationResults.DeckValidationResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DeckValidator {

    private static final Logger logger = LoggerFactory.getLogger(DeckValidator.class);

    private CardsService cardsService;

    @Autowired
    public DeckValidator(CardsService cardsService){
        this.cardsService = cardsService;
    }

    public List<DeckValidationResult> validateDeck(Deck deck){
        logger.info("Validating deck with name: {}, id: {}", deck.getName(), deck.getId());
        List<DeckValidationResult> resultsList = new ArrayList<>();

        resultsList.addAll(validateAmountOfCards(deck));

        resultsList.addAll(validateAmountOfEachCard(deck));

        resultsList.addAll(validateAmountOfLinkCards(deck));

        if(resultsList.isEmpty()){
            resultsList.add(new DeckValidationResult(DeckValidationResults.DECK_IS_VALID));
            logger.info("Deck is valid: {}", deck.getName());
        }
        return resultsList;
    }

    public List<DeckValidationResult> validateAmountOfEachCard(Deck deck) {
        List<DeckValidationResult> resultsList = new ArrayList<>();
        for(DeckCard deckCard:deck.getCards()){
            long cardId = deckCard.getCard().getId();
            int amountOfThisCardOwnedByUser = deck.getUser().getAmountOfCard(cardId);
            if(deckCard.amount > amountOfThisCardOwnedByUser){
                DeckValidationResults tooManyCards = DeckValidationResults.TOO_MANY_CARDS_OF_THIS_KIND;
                resultsList.add(new DeckValidationResult(tooManyCards));
                logger.warn("Invalid deck: {}, more cards then user has, card id: {}, card amount: {}, user have: {}",
                        deck.getName(), cardId, deckCard.amount, amountOfThisCardOwnedByUser);
            }
        }
        return resultsList;
    }

    public List<DeckValidationResult> validateAmountOfCards(Deck deck) {
        List<DeckValidationResult> resultsList = new ArrayList<>();
        int numberOfCardsInDeck = 0;
        for(DeckCard deckCard:deck.getCards()){
            numberOfCardsInDeck += deckCard.getAmount();
        }
        if(numberOfCardsInDeck < CardsConsts.MIN_CARDS_IN_DECK){
            resultsList.add(new DeckValidationResult(DeckValidationResults.NOT_ENOUGH_CARDS));
            logger.warn("Invalid deck: {}, not enough cards. Cards in deck: {}, min number: {}",
                    deck.getName(), numberOfCardsInDeck, CardsConsts.MIN_CARDS_IN_DECK);
        }
        return resultsList;
    }

    public List<DeckValidationResult> validateAmountOfLinkCards(Deck deck) {
        List<DeckValidationResult> resultsList = new ArrayList<>();
        int numberOfCardsInDeck = 0;
        for(DeckCard deckCard:deck.getCards()){
            if(isTooFewLinkCards(deckCard)){
                resultsList.add(new DeckValidationResult(DeckValidationResults.NOT_ENOUGH_LINK_CARDS));
                logger.warn("Invalid deck: {}, not enough link cards. Cards in deck: {}, min number: {}, card id: {}",
                        deck.getName(), numberOfCardsInDeck, CardsConsts.MIN_LINK_CARDS, deckCard.getCard().getId());
            }
        }
        return resultsList;
    }

    private boolean isTooFewLinkCards(DeckCard deckCard) {
        return isLinkType(deckCard) && isLessThanMin(deckCard);
    }

    private boolean isLessThanMin(DeckCard deckCard) {
        return deckCard.getAmount() < CardsConsts.MIN_LINK_CARDS;
    }

    private boolean isLinkType(DeckCard deckCard) {
        return deckCard.getCard().getType().equals(CardTypes.LINK);
    }

}
