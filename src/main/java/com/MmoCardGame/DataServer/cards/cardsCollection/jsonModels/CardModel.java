package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels;

import com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels.enums.CardModelSubtype;
import com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels.enums.CardModelType;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CardModel {
    private int id;
    private List<ActivatedAbilityModel> activatedAbilities;
    private List<Integer> abilities;
    private String description;
    private String name;
    private CardModelType type;
    private CardModelSubtype subtype;
    @JsonProperty("class")
    private String cardClass;
    private String subclass;
    private CostModel cost;
    private StatisticsModel statistics;
}
