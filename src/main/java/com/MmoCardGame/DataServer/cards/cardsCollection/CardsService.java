package com.MmoCardGame.DataServer.cards.cardsCollection;

import com.MmoCardGame.DataServer.cards.CardTypes;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CardsService {

    private CardsRepository cardsRepository;

    @Autowired
    public CardsService(CardsRepository cardsRepository){
        this.cardsRepository = cardsRepository;
    }

    public List<Card> getCardsByType(CardTypes type){
        return cardsRepository.findByType(type);
    }

    public List<Card> getAllCards(){
        return cardsRepository.findAll();
    }

    public Card getCard(long id){
        return cardsRepository.findOne(id);
    }

    public void addCard(Card card){
        cardsRepository.save(card);
    }

    public int getNumberOfCards(){
        return (int) cardsRepository.count();
    }

}
