package com.MmoCardGame.DataServer.cards.cardsCollection;

import com.MmoCardGame.DataServer.cards.CardTypes;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Repository
@Transactional
public interface CardsRepository extends JpaRepository<Card, Long> {
    List<Card> findByType(CardTypes type);
}
