package com.MmoCardGame.DataServer.cards.cardsCollection;

import com.MmoCardGame.DataServer.cards.cardsEntities.Card;

import java.util.List;

public interface CardsLoader {
    List<Card> loadCards();
}
