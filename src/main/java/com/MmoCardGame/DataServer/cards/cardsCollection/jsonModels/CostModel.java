package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels;

import lombok.Data;

@Data
public class CostModel {
    private int neutral;
    private int raiders;
    private int scanners;
    private int whiteHats;
    private int psychotronnics;
}
