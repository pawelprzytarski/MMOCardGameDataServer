package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels.enums;

public enum CardModelType {
    Link, Unit, Script, Operation, ServiceProgram
}
