package com.MmoCardGame.DataServer.cards.cardsEntities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;

@Entity(name = "DECKS_CARDS")
@Data
@NoArgsConstructor
@ToString(exclude = {"deck", "card"})
@EqualsAndHashCode(of = "id")
public class DeckCard {

    @EmbeddedId
    private DeckCardId id = new DeckCardId();

    @ManyToOne
    @JoinColumn(name = "DECK_ID")
    @MapsId("deckId")
    private Deck deck;

    @ManyToOne
    @JoinColumn(name = "CARD_ID")
    @MapsId("cardId")
    private Card card;

    public int amount;

    public DeckCard(Deck deck, Card card){
        this.card = card;
        this.deck = deck;
        amount = 1;
    }

    public void increaseAmount(){
        amount++;
    }

    public void decreaseAmount(){
        amount--;
    }
}
