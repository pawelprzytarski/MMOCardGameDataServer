package com.MmoCardGame.DataServer.cards.cardsEntities;

import com.MmoCardGame.DataServer.users.User;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;

@Entity(name = "USERS_CARDS")
@Data
@NoArgsConstructor
@ToString(exclude = {"user", "card"})
@EqualsAndHashCode(of = "id")
public class UserCard implements Serializable {

    @EmbeddedId
    private UserCardId id = new UserCardId();

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    @MapsId("userId")
    private User user;

    @ManyToOne
    @JoinColumn(name = "CARD_ID")
    @MapsId("cardId")
    private Card card;

    public int amount;

    public UserCard(User user, Card card){
        this.card = card;
        this.user = user;
        amount = 1;
    }

    public void increaseAmount(){
        amount++;
    }

    public void decreaseAmount(){
        amount--;
    }
}
