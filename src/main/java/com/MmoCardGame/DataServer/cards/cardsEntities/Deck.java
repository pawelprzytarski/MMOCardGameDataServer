package com.MmoCardGame.DataServer.cards.cardsEntities;

import com.MmoCardGame.DataServer.REST.jsonMessages.CardWithAmount;
import com.MmoCardGame.DataServer.users.User;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity(name = "DECKS")
@Data
@NoArgsConstructor
@ToString(exclude = {"user"})
@EqualsAndHashCode(of = {"id", "name"})
@JsonIgnoreProperties({ "user", "cards" })
public class Deck {

    private static final Logger logger = LoggerFactory.getLogger(Deck.class);

    @Id
    @GeneratedValue
    private Long id;

    private String name;

    @OneToMany(mappedBy = "id.deckId", cascade = CascadeType.ALL)
    private List<DeckCard> cards = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User user;

    public int getAmountOfCard(long id){
        int amountInDeck = 0;
        for(DeckCard userCard : cards){
            if(userCard.getCard().getId().equals(id)){
                amountInDeck = userCard.getAmount();
                break;
            }
        }
        return amountInDeck;
    }

    public List<CardWithAmount> getCardsWithAmounts(){
        return getCards()
                .stream()
                .map(deckCard ->
                        new CardWithAmount(deckCard.getCard().getId(), deckCard.getAmount())
                )
                .collect(Collectors.toList());
    }

    public void addCard(Card card){
        DeckCard deckCard = tryGetDeckCardRelation(card);
        if(deckCard != null) {
            increaseAmount(deckCard);
        } else {
            addNewDeckCardEntity(card);
        }
    }

    private void increaseAmount(DeckCard deckCard) {
        deckCard.increaseAmount();
        logger.info("Increased amount in deck card relation: {}, for deck: {}", deckCard, name);
    }

    private DeckCard tryGetDeckCardRelation(Card newCard) {
        DeckCard deckCard = null;
        for(DeckCard relation : getCards()){
            if(Objects.equals(relation.getCard().getId(), newCard.getId())){
                deckCard = relation;
            }
        }
        return deckCard;
    }

    private void addNewDeckCardEntity(Card newCard) {
        DeckCard deckCard = new DeckCard(this, newCard);
        getCards().add(deckCard);
        newCard.getDecks().add(deckCard);
        logger.info("Added new deck card relation: {}, for deck: {}", deckCard, name);
    }

    public void removeCard(Card card) {
        DeckCard deckCard = tryGetDeckCardRelation(card);
        if(deckCard != null) {
            decreaseAmount(deckCard);
        }
    }

    private void decreaseAmount(DeckCard deckCard) {
        deckCard.decreaseAmount();
        logger.info("Decreased amount in deck card relation: {}, for deck: {}", deckCard, name);
    }
}
