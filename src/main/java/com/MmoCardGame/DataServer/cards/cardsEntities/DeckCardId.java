package com.MmoCardGame.DataServer.cards.cardsEntities;


import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
@Data
@NoArgsConstructor
public class DeckCardId implements Serializable {

    @Column(name = "card_id")
    public Long cardId;

    @Column(name = "deck_id")
    public Long deckId;

}
