package com.MmoCardGame.DataServer.cards.configuration;

import com.MmoCardGame.DataServer.cards.packets.CardsPacketOpener;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsFromJsonLoader;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsLoader;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsService;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EnableJpaRepositories("com.MmoCardGame.DataServer.cards")
public class CardsBeansConf {

    @Bean(name = "cardsJsonFilePath")
    public String cardsJsonFilePath(){
        return CardsConsts.JSON_CARDS_FILE_PATH;
    }

    @Bean
    public CardsPacketOpener cardsPacketOpener(CardsService cardsService){
        return new CardsPacketOpener(cardsService);
    }

    @Bean
    public CommandLineRunner initCards(CardsService cardsService) {
        return (evt) -> {
            for(Card card : cardsLoader().loadCards()) {
                cardsService.addCard(card);
            }
        };
    }

    @Bean
    public CardsLoader cardsLoader(){
        return new CardsFromJsonLoader(cardsJsonFilePath());
    }
}
