package com.MmoCardGame.DataServer.cards.packets;

import com.MmoCardGame.DataServer.cards.cardsCollection.CardsService;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Component
public class CardsPacketOpener {

    private static final Logger logger = LoggerFactory.getLogger(CardsPacketOpener.class);

    private CardsService cardsService;

    @Autowired
    public CardsPacketOpener(CardsService cardsService){
        this.cardsService = cardsService;
    }

    private Random random = new Random();

    public List<Card> openPacket(PacketsTypes packetType){
        List<Card> randomCards = new ArrayList<>();
        for(int i = 0; i < PacketsTypes.getNumberOfCards(packetType); i++) {
            Card randomCard = randomCard();
            randomCards.add(randomCard);
            logger.info("Random card id in opened packet: {}", randomCard.getId());
        }
        return randomCards;
    }

    private Card randomCard() {
        List<Card> cardList = cardsService.getAllCards();
        int numberOfAvailableCards = cardList.size();
        int randomIndex = random.nextInt(numberOfAvailableCards);
        return cardList.get(randomIndex);
    }

}
