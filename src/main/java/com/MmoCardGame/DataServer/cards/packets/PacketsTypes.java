package com.MmoCardGame.DataServer.cards.packets;

import com.MmoCardGame.DataServer.cards.configuration.CardsConsts;
import com.MmoCardGame.DataServer.exceptions.NoSuchPacketTypeException;

public enum PacketsTypes {
    SMALL_PACKET, BIG_PACKET, MEDIUM_PACKET;

    public static int getNumberOfCards(PacketsTypes type){
        if(type.equals(SMALL_PACKET)){
            return CardsConsts.NUMBER_OF_CARDS_IN_SMALL_PACKET;
        } else if(type.equals(MEDIUM_PACKET)){
            return CardsConsts.NUMBER_OF_CARDS_IN_MEDIUM_PACKET;
        } else if(type.equals(BIG_PACKET)){
            return CardsConsts.NUMBER_OF_CARDS_IN_BIG_PACKET;
        }

        throw new NoSuchPacketTypeException(type);
    }
}
