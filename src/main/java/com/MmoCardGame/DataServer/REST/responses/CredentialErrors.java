package com.MmoCardGame.DataServer.REST.responses;

public class CredentialErrors {
    public static final String INVALID_EMAIL_MESSAGE = "Email is not valid.";
    public static final String INVALID_LOGIN_MESSAGE = "Login is not valid.";
    public static final String INVALID_PASSWORD_MESSAGE = "Password is not valid.";

    public static final String FIELDS_DONT_MATCH_MESSAGE = "Fields are not equal.";
    public static final String PASSWORDS_DONT_MATCH_MESSAGE = "Passwords are not equal.";
    public static final String EMAILS_DONT_MATCH_MESSAGE = "Emails are not equal.";

    public static final String LOGIN_USED_MESSAGE = "Login already used.";
    public static final String EMAIL_USED_MESSAGE = "Email already used.";

    public static final String WRONG_PASSWORD_MESSAGE = "Not user password.";
}
