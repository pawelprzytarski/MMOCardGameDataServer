package com.MmoCardGame.DataServer.REST.utilConstants;

public class ApiPaths {
    private static final String API_URL = "";

    public static final String USERS_URL = API_URL + "/users";
    public static final String LOGGED_USERS_URL = API_URL + "/users/authenticated";
    public static final String ADMIN_URL = API_URL + "/admin";
    public static final String ADMIN_USERS_URL = API_URL + "/admin/users";
    public static final String SHOP_URL = API_URL + "/shop";
    public static final String GAME_PLAYS_URL = API_URL + "/gamePlays";
    public static final String SERVER_URL = "/server";

    //swagger-ui urls. Ignored by spring security
    public static final String SWAGGER_URL = "/api-docs/swagger";
    public static final String SWAGGER_UI_HTML_URL = "/swagger-ui.html";
    public static final String WEBJARS_URL = "/webjars";
    public static final String SWAGGER_RESOURCES_URL = "/swagger-resources";
}
