package com.MmoCardGame.DataServer.REST.jsonMessages;

import com.MmoCardGame.DataServer.game.data.GamePlayResults;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EndGamePlayInfo {
    private GamePlayResults result;
    private String additionalInfo;
    private String userLogin;
    private int sessionId;
}