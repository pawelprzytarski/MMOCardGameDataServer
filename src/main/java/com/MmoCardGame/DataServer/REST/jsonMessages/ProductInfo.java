package com.MmoCardGame.DataServer.REST.jsonMessages;

import com.MmoCardGame.DataServer.shop.products.ProductNames;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductInfo {
    private ProductNames name;
}
