package com.MmoCardGame.DataServer.REST.controllers.game;

import com.MmoCardGame.DataServer.game.results.GamePlayResult;
import com.MmoCardGame.DataServer.REST.jsonMessages.GamePlayStartData;
import com.MmoCardGame.DataServer.REST.jsonMessages.UserGamePlayInfo;
import com.MmoCardGame.DataServer.REST.responses.ResponseMessages;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.game.GamePlaysService;
import com.MmoCardGame.DataServer.game.results.LastGamePlaysResultsService;
import com.MmoCardGame.DataServer.game.data.GamePlay;
import com.MmoCardGame.DataServer.game.data.GamePlayStartRequest;
import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GamePlayController {

    private static final Logger logger = LoggerFactory.getLogger(GamePlayController.class);

    private GamePlaysService gamesService;
    private UsersService usersService;
    private LastGamePlaysResultsService lastGamePlaysResults;

    @Autowired
    public GamePlayController(GamePlaysService gamesService,
                              UsersService usersService,
                              LastGamePlaysResultsService lastGamePlaysResults){
        this.gamesService = gamesService;
        this.usersService = usersService;
        this.lastGamePlaysResults = lastGamePlaysResults;
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}/game/lastResult")
    public ResponseEntity getUserLastGamePlayResult(@PathVariable String login){
        if(!lastGamePlaysResults.hasResult(login)){
            logger.warn("Couldn't get user game play result because user doesn't have game play result. Login: {}", login);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.NO_GAME_PLAY_RESULT_FOR_USER);
        }
        GamePlayResult result = lastGamePlaysResults.getResult(login);
        logger.info("Getting user last game play result. Login: {}, result: {}", login, result);
        return UtilResponsesBuilder.buildOkResponseWithBody(result);
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}/game/exist")
    public Boolean checkIfGameForPlayerExist(@PathVariable String login){
        boolean userHasGame = gamesService.userHasGame(login);
        logger.info("Checking if user has game. Login: {}, result: {}", login, userHasGame);
        return userHasGame;
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}/game")
    public ResponseEntity getUserGamePlayInfo(@PathVariable String login){
        if(!gamesService.userHasGame(login)){
            logger.warn("Couldn't get user game play info because user doesn't have game. Login: {}", login);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.NO_GAME_PLAY_FOR_USER);
        }
        GamePlay gamePlayForUser = gamesService.getGamePlayForUser(login);

        UserGamePlayInfo gamePlayInfo = new UserGamePlayInfo(gamePlayForUser, login);
        logger.info("Getting user game play info. Login: {}, game play info: {}", login, gamePlayInfo);
        return UtilResponsesBuilder.buildOkResponseWithBody(gamePlayInfo);
    }

    @PostMapping(ApiPaths.USERS_URL + "/{login}/game/{gameType}")
    public ResponseEntity startGamePlayRequest(@PathVariable String login,
                                               @PathVariable GamePlayTypes gameType){
        User user = usersService.getUserByLoginWithCards(login);

        GamePlayStartRequest gameStartRequest = new GamePlayStartRequest(user, gameType);

        if(gamesService.isUserWaiting(user)){
            logger.warn("Couldn't start game play because user is already waiting for game. Login: {}", login);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.PRECONDITION_FAILED, ResponseMessages.USER_ALREADY_WAITING);
        }
        if(gamesService.userHasGame(login)){
            logger.warn("Couldn't start game play because user already has game. Login: {}", login);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.PRECONDITION_FAILED, ResponseMessages.USER_ALREADY_HAS_GAME);
        }
        gamesService.dispatchGameStartRequest(gameStartRequest);

        logger.info("Start game play request handled for user. Login: {}", login);
        return UtilResponsesBuilder.buildOkResponseWithBody(ResponseMessages.GAME_START_REQUEST_APPROVED);
    }

    @PostMapping(ApiPaths.USERS_URL + "/{login}/game")
    public ResponseEntity startGamePlayRequestWithDeckSelection(@PathVariable String login,
                                                                @RequestBody GamePlayStartData gamePlayStartData){
        User user = usersService.getUserByLoginWithCards(login);

        GamePlayStartRequest gameStartRequest = new GamePlayStartRequest(user, gamePlayStartData.getGamePlayType());

        if(gamesService.isUserWaiting(user)){
            logger.warn("Couldn't start game play with deck selection because user is already waiting for game. Login: {}, Deck: {}", login, gamePlayStartData.getDeckName());
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.PRECONDITION_FAILED, ResponseMessages.USER_ALREADY_WAITING);
        }
        if(gamesService.userHasGame(login)){
            logger.warn("Couldn't start game play with deck selection because user already has game. Login: {}, Deck: {}", login, gamePlayStartData.getDeckName());
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.PRECONDITION_FAILED, ResponseMessages.USER_ALREADY_HAS_GAME);
        }
        if(!user.hasDeck(gamePlayStartData.getDeckName())){
            logger.warn("Couldn't start game play with deck selection because user doesn't have deck. Login: {}, Deck: {}", login, gamePlayStartData.getDeckName());
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.DECK_DOES_NOT_EXIST);
        }

        user.setSelectedDeckName(gamePlayStartData.getDeckName());
        gamesService.dispatchGameStartRequest(gameStartRequest);

        logger.info("Start game play request for user with deck selection handled. Login: {}, Deck: {}", login, gamePlayStartData.getDeckName());
        return UtilResponsesBuilder.buildOkResponseWithBody(ResponseMessages.GAME_START_REQUEST_APPROVED);
    }

    @DeleteMapping(ApiPaths.USERS_URL + "/{login}/game")
    public ResponseEntity stopWaitingForGame(@PathVariable String login){
        User user = usersService.getUserByLogin(login);

        gamesService.removeWaitingPlayer(user);

        logger.info("User canceled waiting for game play. Login: {}", login);

        return UtilResponsesBuilder.buildOkResponseWithBody(ResponseMessages.QUIT_FROM_QUEUE);
    }

}
