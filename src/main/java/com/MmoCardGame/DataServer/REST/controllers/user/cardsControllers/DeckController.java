package com.MmoCardGame.DataServer.REST.controllers.user.cardsControllers;

import com.MmoCardGame.DataServer.REST.jsonMessages.CardInfo;
import com.MmoCardGame.DataServer.REST.jsonMessages.CardWithAmount;
import com.MmoCardGame.DataServer.REST.jsonMessages.CreateDeck;
import com.MmoCardGame.DataServer.REST.responses.ResponseMessages;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsService;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import com.MmoCardGame.DataServer.cards.cardsEntities.Deck;
import com.MmoCardGame.DataServer.game.GamePlaysService;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@RestController
public class DeckController {

    private static final Logger logger = LoggerFactory.getLogger(DeckController.class);

    private UsersService usersService;

    private CardsService cardsService;
    private GamePlaysService gamesService;

    @Autowired
    public DeckController(UsersService usersService, CardsService cardsService, GamePlaysService gamesService){
        this.cardsService = cardsService;
        this.usersService = usersService;
        this.gamesService = gamesService;
    }

    @PostMapping(ApiPaths.USERS_URL + "/{login}/decks/{deckName}")
    public ResponseEntity addCardToDeck(@RequestBody CardInfo cardInfo,
                                        @PathVariable String login,
                                        @PathVariable String deckName){
        User user = usersService.getUserByLogin(login);

        Deck deck = user.getDeck(deckName);
        if(deck == null){
            logger.warn("Trying to add card to non existing deck. Deck name: {}, login: {}, card id: {}", login, deckName, cardInfo.getId());
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.DECK_DOES_NOT_EXIST);
        }

        Card card;

        try {
            card = addCardToDeck(cardInfo, login, deckName, user, deck);
        }
        catch (Exception e){
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.PRECONDITION_FAILED, e.getMessage());
        }

        usersService.updateUser(user);

        return UtilResponsesBuilder.buildOkResponseWithBody(card);
    }

    private Card addCardToDeck(@RequestBody CardInfo cardInfo, @PathVariable String login, @PathVariable String deckName, User user, Deck deck) throws Exception {
        Card card = cardsService.getCard(cardInfo.getId());

        if (card == null) {
            logger.warn("Trying to add non existing card to deck. Deck name: {}, login: {}, card id: {}", login, deckName, cardInfo.getId());
            throw new Exception(ResponseMessages.CARD_DOES_NOT_EXIST);

        }

        int amountInUsersCollection = user.getAmountOfCard(cardInfo.getId());
        int amountInDeck = deck.getAmountOfCard(cardInfo.getId());

        if (amountInDeck >= amountInUsersCollection) {
            logger.warn("Trying to add card to deck, while user doesn't have any more. Deck name: {}, login: {}, card id: {}, user has: {}", login, deckName, cardInfo.getId(), amountInUsersCollection);
            throw new Exception(ResponseMessages.NOT_ENOUGH_CARDS);
        }

        deck.addCard(card);
        logger.info("Successfully added card to deck. Deck name: {}, login: {}, card id: {}", login, deckName, cardInfo.getId());

        return card;
    }

    @PostMapping(ApiPaths.USERS_URL + "/{login}/decks/{deckName}/cards")
    public ResponseEntity bulkAddCardToDeck(@RequestBody List<CardWithAmount> cardsInfo,
                                        @PathVariable String login,
                                        @PathVariable String deckName){
        User user = usersService.getUserByLogin(login);

        Deck deck = user.getDeck(deckName);
        if(deck == null){
            logger.warn("Trying to bulk add card to non existing deck. Deck name: {}, login: {}", login, deckName);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.DECK_DOES_NOT_EXIST);
        }

        try {
            for(CardWithAmount cardInfo:cardsInfo) {
                CardInfo cardId = new CardInfo(cardInfo.getCardId());
                for (int i = 0; i < cardInfo.getAmount(); i++)
                    addCardToDeck(cardId, login, deckName, user, deck);
            }
        }
        catch (Exception e){
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.PRECONDITION_FAILED, e.getMessage());
        }

        usersService.updateUser(user);

        return UtilResponsesBuilder.buildOkResponse();
    }

    @DeleteMapping(ApiPaths.USERS_URL + "/{login}/decks/{deckName}/cards")
    public ResponseEntity bulkDeleteCardFromDeck(@RequestBody List<CardWithAmount> cardsInfo,
                                            @PathVariable String login,
                                            @PathVariable String deckName){
        User user = usersService.getUserByLogin(login);

        Deck deck = user.getDeck(deckName);
        if(deck == null){
            logger.warn("Trying to bulk add card to non existing deck. Deck name: {}, login: {}", login, deckName);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.DECK_DOES_NOT_EXIST);
        }

        try {
            for(CardWithAmount cardWithAmount:cardsInfo) {
                CardInfo cardInfo = new CardInfo(cardWithAmount.getCardId());
                for (int i = 0; i < cardWithAmount.getAmount(); i++) {

                    Card card = cardsService.getCard(cardInfo.getId());

                    if (card == null) {
                        logger.warn("Trying to add non existing card to deck. Deck name: {}, login: {}, card id: {}", login, deckName, cardInfo.getId());
                        throw new Exception(ResponseMessages.CARD_DOES_NOT_EXIST);

                    }

                    int amountInDeck = deck.getAmountOfCard(cardInfo.getId());

                    if (amountInDeck <= 0) {
                        logger.warn("Trying to remove card from deck, while user doesn't have any card this type in deck. Deck name: {}, login: {}, card id: {}", login, deckName, cardInfo.getId());
                        throw new Exception(ResponseMessages.NOT_ENOUGH_CARDS);
                    }

                    deck.removeCard(card);
                    logger.info("Successfully removed card from deck. Deck name: {}, login: {}, card id: {}", login, deckName, cardInfo.getId());
                }
            }
        }
        catch (Exception e){
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.PRECONDITION_FAILED, e.getMessage());
        }

        usersService.updateUser(user);

        return UtilResponsesBuilder.buildOkResponse();
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}/decks/{deckName}")
    public ResponseEntity getDeckCards(@PathVariable String login,
                                       @PathVariable String deckName){
        User user = usersService.getUserByLogin(login);

        Deck deck = user.getDeck(deckName);

        if(deck == null){
            logger.warn("Getting deck cards of non existing deck. Deck name: {}, login: {}", login, deckName);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.DECK_DOES_NOT_EXIST);
        }

        List<CardWithAmount> cards = deck.getCardsWithAmounts();

        logger.info("Getting deck cards. Deck name: {}, login: {}, deck cards: {}", login, deckName, cards);
        return UtilResponsesBuilder.buildOkResponseWithBody(cards);
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}/decks")
    public ResponseEntity getDecks(@PathVariable String login){
        User user = usersService.getUserByLogin(login);

        Set<Deck> decks = user.getDecks();
        logger.info("Getting user decks. login: {}, decks: {}", login, decks);

        return UtilResponsesBuilder.buildOkResponseWithBody(decks);
    }

    @PostMapping(ApiPaths.USERS_URL + "/{login}/decks")
    public ResponseEntity createNewDeck(@PathVariable String login,
                                        @RequestBody CreateDeck createDeck){
        User user = usersService.getUserByLogin(login);

        if(user.hasDeck(createDeck.getName())) {
            logger.warn("Trying to create new deck for user while name is already taken. login: {}, deck name: {}", login, createDeck.getName());
            return UtilResponsesBuilder.buildBadRequestResponseWithError(ResponseMessages.DECK_ALREADY_EXISTS);
        }

        Deck newDeck = new Deck();
        newDeck.setName(createDeck.getName());
        user.addDeck(newDeck);

        usersService.updateUser(user);
        logger.info("Creating new deck for user. login: {}, deck name: {}", login, createDeck.getName());

        return UtilResponsesBuilder.buildCreatedResponse();
    }

    @DeleteMapping(ApiPaths.USERS_URL + "/{login}/decks/{deckName}")
    public ResponseEntity removeDeck(@PathVariable String login,
                                     @PathVariable String deckName){
        User user = usersService.getUserByLogin(login);

        if(!user.hasDeck(deckName)){
            logger.warn("Trying to remove non existing user deck. login: {}, deck name: {}", login, deckName);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.DECK_DOES_NOT_EXIST);
        }

        user.removeDeck(deckName);
        usersService.updateUser(user);
        logger.info("Removed user deck. login: {}, deck name: {}", login, deckName);

        return UtilResponsesBuilder.buildOkResponseWithBody(ResponseMessages.DECK_REMOVED);
    }

    @PutMapping(ApiPaths.USERS_URL + "/{login}/decks/{deckName}")
    public ResponseEntity selectDeckToUse(@PathVariable String login,
                                          @PathVariable String deckName){
        User user = usersService.getUserByLogin(login);

        if(gamesService.isUserWaiting(user)){
            logger.warn("Trying to choose deck to play with while user is in queue. login: {}, deck name: {}", login, deckName);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.PRECONDITION_FAILED, ResponseMessages.CANT_SELECT_DECK_WHILE_IN_QUEUE);
        }

        if(gamesService.userHasGame(user.getLogin())){
            logger.warn("Trying to choose deck to play with while user has game. login: {}, deck name: {}", login, deckName);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.PRECONDITION_FAILED, ResponseMessages.CANT_SELECT_DECK_WHILE_IN_GAME);
        }

        if(!user.hasDeck(deckName)){
            logger.warn("Trying to choose deck to play with while user does not have one. login: {}, deck name: {}", login, deckName);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.DECK_DOES_NOT_EXIST);
        }

        user.setSelectedDeckName(deckName);
        usersService.updateUser(user);

        logger.info("Chosen deck to play with by user. login: {}, deck name: {}", login, deckName);
        return UtilResponsesBuilder.buildOkResponseWithBody(ResponseMessages.DECK_SELECTED);
    }

}
