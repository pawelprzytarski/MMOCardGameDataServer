package com.MmoCardGame.DataServer.REST.controllers.user;

import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
public class UserController {

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    private UsersService usersService;

    @Autowired
    public UserController(UsersService usersService){
        this.usersService = usersService;
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}/packetsAmount")
    public ResponseEntity getNumberOfPackets(@PathVariable String login) {
        User user = usersService.getUserByLogin(login);
        Map<PacketsTypes, Integer> numberOfCardPackets = user.getNumberOfCardPackets();
        logger.info("Returning user packets amount. User login: {}, Packets amount: {}", login, numberOfCardPackets);
        return UtilResponsesBuilder.buildOkResponseWithBody(numberOfCardPackets);
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}/packetsAmount/{packetType}")
    public ResponseEntity getNumberOfSpecifiedPackets(@PathVariable String login,
                                                      @PathVariable PacketsTypes packetType) {
        User user = usersService.getUserByLogin(login);
        Integer amount = user.getNumberOfCardPackets().get(packetType);
        logger.info("Returning user packets amount. User login: {}, Packets amount: {}, Packets type: {}", login, amount, packetType);
        return UtilResponsesBuilder.buildOkResponseWithBody(amount);
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}/moneyAmount")
    public ResponseEntity getMoneyAmount(@PathVariable String login) {
        User user = usersService.getUserByLogin(login);
        int money = user.getMoney();
        logger.info("Returning user money. User login: {}, Money: {}", login, money);
        return UtilResponsesBuilder.buildOkResponseWithBody(money);
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}")
    public ResponseEntity getUserInfo(@PathVariable String login) {
        User user = usersService.getUserByLogin(login);
        user.setPassword("***********");
        logger.info("Returning user info. User login: {}, Info: {}", login, user);
        return UtilResponsesBuilder.buildOkResponseWithBody(user);
    }

}
