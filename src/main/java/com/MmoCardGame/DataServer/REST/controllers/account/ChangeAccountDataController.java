package com.MmoCardGame.DataServer.REST.controllers.account;


import com.MmoCardGame.DataServer.REST.jsonMessages.ChangeEmailData;
import com.MmoCardGame.DataServer.REST.jsonMessages.ChangePasswordData;
import com.MmoCardGame.DataServer.REST.responses.CredentialErrors;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.users.UsersService;
import com.MmoCardGame.DataServer.users.resetPasswordTokens.ResetPasswordTokensService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class ChangeAccountDataController {

    private static final Logger logger = LoggerFactory.getLogger(ChangeAccountDataController.class);

    private final UsersService usersService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ChangeAccountDataController(UsersService usersService, ResetPasswordTokensService passwordTokensService, PasswordEncoder passwordEncoder){
        this.usersService = usersService;
        this.passwordEncoder = passwordEncoder;
    }

    @PutMapping(ApiPaths.USERS_URL +"/{login}/password")
    public ResponseEntity changePassword(@RequestBody @Valid ChangePasswordData changePasswordData,
                                         BindingResult bindingResult,
                                         @PathVariable String login){

        if(bindingResult.hasErrors()){
            logger.warn("Trying to change password with invalid data. Login: {}, Data: {}", login, changePasswordData);
            return UtilResponsesBuilder.buildValidationErrorResponse(bindingResult);
        }

        if(!usersService.getUserByLogin(login).getPassword().equals(changePasswordData.getPassword())){
            logger.warn("Trying to change password with wrong old password. Login: {}, Data: {}", login, changePasswordData);
            return UtilResponsesBuilder.buildBadRequestResponseWithError(CredentialErrors.WRONG_PASSWORD_MESSAGE);
        }

        String newPassword = changePasswordData.getNewPassword();
        usersService.updatePasswordFor(passwordEncoder.encode(newPassword), login);

        logger.info("Successfully changed password. Login: {}, Data: {}", login, changePasswordData);
        return UtilResponsesBuilder.buildOkResponse();
    }

    @PutMapping(ApiPaths.USERS_URL +"/{login}/email")
    public ResponseEntity changeEmail(@RequestBody @Valid ChangeEmailData changeEmailData,
                                      BindingResult bindingResult,
                                      @PathVariable String login){

        ResponseEntity errorResponse = checkForEmailChangeErrorsAndGetResponse(changeEmailData, bindingResult);
        if (errorResponse != null) {
            logger.warn("Trying to change email with errors. Login: {}, Data: {}, Error: {}", login, changeEmailData, errorResponse);
            return errorResponse;
        }

        if(!usersService.getUserByLogin(login).getPassword().equals(changeEmailData.getPassword())){
            logger.warn("Trying to change email with wrong password. Login: {}, Data: {}", login, changeEmailData);
            return UtilResponsesBuilder.buildBadRequestResponseWithError(CredentialErrors.WRONG_PASSWORD_MESSAGE);
        }

        usersService.updateEmailFor(changeEmailData.getNewEmail(), login);

        logger.info("Successfully changed email. Login: {}, Data: {}", login, changeEmailData);
        return UtilResponsesBuilder.buildOkResponse();
    }

    private ResponseEntity checkForEmailChangeErrorsAndGetResponse(
            ChangeEmailData changeEmailData,
            BindingResult bindingResult) {
        if(bindingResult.hasErrors()){
            return UtilResponsesBuilder.buildValidationErrorResponse(bindingResult);
        }

        List<String> errors = getCredentialUsageErrorsForEmail(changeEmailData);
        if(!errors.isEmpty()){
            return UtilResponsesBuilder.buildBadRequestResponseWithErrors(errors);
        }
        return null;
    }

    private List<String> getCredentialUsageErrorsForEmail(ChangeEmailData changeEmailData){
        List<String> errors = new ArrayList<>();
        if(usersService.isEmailUsed(changeEmailData.getNewEmail())){
            errors.add(CredentialErrors.EMAIL_USED_MESSAGE);
        }
        return errors;
    }

}
