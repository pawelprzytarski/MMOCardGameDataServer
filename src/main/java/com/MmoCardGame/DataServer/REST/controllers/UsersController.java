package com.MmoCardGame.DataServer.REST.controllers;

import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.REST.utilConstants.ContentTypes;
import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersRepository;
import com.MmoCardGame.DataServer.REST.responses.CredentialErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

//In WebSecurityContext access to this controller is allowed only for Admin users.
@RestController
public class UsersController {

    private static final Logger logger = LoggerFactory.getLogger(UsersController.class);

    private UsersRepository usersRepository;
    private PasswordEncoder passwordEncoder;

    @Autowired
    public UsersController(UsersRepository usersRepository, PasswordEncoder passwordEncoder){
        this.usersRepository = usersRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(ApiPaths.ADMIN_USERS_URL + "/{login}/packets/{packetType}")
    public ResponseEntity addPacket(@PathVariable String login,
                                    @PathVariable PacketsTypes packetType){
        User user = usersRepository.findByLogin(login);
        user.addPacket(packetType);
        logger.info("Adding packet to user. Packet type: {}, user login: {}", packetType, login);
        return UtilResponsesBuilder.buildOkResponse();
    }

    @GetMapping(ApiPaths.ADMIN_USERS_URL)
    public Collection<User> getUsers(){
        Collection<User> users = new ArrayList<>();
        users.addAll(usersRepository.findAll());
        logger.info("Getting all users: {}", users);
        return users;
    }

    @GetMapping(ApiPaths.ADMIN_USERS_URL + "/{id}")
    public ResponseEntity getUser(@PathVariable long id){
        User user = usersRepository.findOne(id);
        if(user == null) {
            logger.warn("Trying to get user, but not found with id: {}", id);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        logger.info("Getting user, with id: {}, and login: {}", id, user.getLogin());
        return ResponseEntity
                .status(HttpStatus.OK)
                .contentType(ContentTypes.JSON_UTF8)
                .body(user);
    }

    @PostMapping(value = ApiPaths.ADMIN_USERS_URL, consumes = "application/json")
    public ResponseEntity addUser(@RequestBody User user){

        List<String> errors = getCredentialUsageErrors(user);
        if(!errors.isEmpty()){
            logger.warn("Trying to add already existing user, login: {}, email: {}, id: {}", user.getLogin(), user.getEmail(), user.getId());
            return buildConflictResponse(errors);
        }


        user.setPassword(passwordEncoder.encode(user.getPassword()));

        logger.info("Adding user, login: {}, email: {}, id: {}", user.getLogin(), user.getEmail(), user.getId());

        usersRepository.save(user);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    private ResponseEntity buildConflictResponse(List<String> errors) {
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .contentType(ContentTypes.JSON_UTF8)
                .body(errors);
    }

    private List<String> getCredentialUsageErrors(User user){
        List<String> errors = new ArrayList<>();
        if(isEmailUsed(user.getEmail())){
            errors.add(CredentialErrors.EMAIL_USED_MESSAGE);
        }
        if(isLoginUsed(user.getLogin())){
            errors.add(CredentialErrors.LOGIN_USED_MESSAGE);
        }
        return errors;
    }

    private boolean isEmailUsed(String email){
        User user = usersRepository.findByEmail(email);
        return user != null;
    }

    private boolean isLoginUsed(String login){
        User user = usersRepository.findByLogin(login);
        return user != null;
    }


}
