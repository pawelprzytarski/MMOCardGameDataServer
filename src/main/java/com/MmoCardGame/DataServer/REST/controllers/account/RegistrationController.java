package com.MmoCardGame.DataServer.REST.controllers.account;

import com.MmoCardGame.DataServer.REST.jsonMessages.RegistrationData;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import com.MmoCardGame.DataServer.REST.responses.CredentialErrors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

//In WebSecurityContext access to this controller is allowed for every client.
@RestController
public class RegistrationController {

    private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

    private final UsersService usersService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public RegistrationController(UsersService usersService, PasswordEncoder passwordEncoder){
        this.usersService = usersService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(ApiPaths.USERS_URL)
    public ResponseEntity register(
            @RequestBody @Valid RegistrationData registrationData,
            BindingResult bindingResult){

        if(bindingResult.hasErrors()){
            logger.warn("Trying to register user with validation errors. Registration data: {}, errors: {}", registrationData, bindingResult.getAllErrors());
            return UtilResponsesBuilder.buildValidationErrorResponse(bindingResult);
        }

        List<String> errors = getCredentialUsageErrors(registrationData);
        if(!errors.isEmpty()){
            logger.warn("Trying to register user with user credentials errors. Registration data: {}, errors: {}", registrationData, errors);
            return UtilResponsesBuilder.buildBadRequestResponseWithErrors(errors);
        }

        registrationData.setPassword(passwordEncoder.encode(registrationData.getPassword()));
        User user = new User(registrationData);
        usersService.addUser(user);

        logger.info("Successfully registered user: {}, registration data: {}", user, registrationData);

        return UtilResponsesBuilder.buildCreatedResponse();
    }

    private List<String> getCredentialUsageErrors(RegistrationData registrationData){
        List<String> errors = new ArrayList<>();
        if(usersService.isEmailUsed(registrationData.getEmail())){
            errors.add(CredentialErrors.EMAIL_USED_MESSAGE);
        }
        if(usersService.isLoginUsed(registrationData.getLogin())){
            errors.add(CredentialErrors.LOGIN_USED_MESSAGE);
        }
        return errors;
    }

}
