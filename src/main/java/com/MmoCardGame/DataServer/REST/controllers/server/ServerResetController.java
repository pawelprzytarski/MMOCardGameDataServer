package com.MmoCardGame.DataServer.REST.controllers.server;

import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.game.GamePlaysService;
import com.MmoCardGame.DataServer.game.playersQueues.PlayersQueuesCollection;
import com.MmoCardGame.DataServer.game.results.LastGamePlaysResultsService;
import com.MmoCardGame.DataServer.security.tokenAuthentication.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServerResetController {

    private static final Logger logger = LoggerFactory.getLogger(ServerResetController.class);

    private TokenService tokenService;
    private PlayersQueuesCollection playersQueuesCollection;
    private GamePlaysService gamePlaysService;
    private LastGamePlaysResultsService lastGamePlaysResultsService;

    @Autowired
    public ServerResetController(TokenService tokenService, PlayersQueuesCollection playersQueuesCollection, GamePlaysService gamePlaysService, LastGamePlaysResultsService lastGamePlaysResultsService) {
        this.tokenService = tokenService;
        this.playersQueuesCollection = playersQueuesCollection;
        this.gamePlaysService = gamePlaysService;
        this.lastGamePlaysResultsService = lastGamePlaysResultsService;
    }

    @DeleteMapping(ApiPaths.SERVER_URL + "/reset")
    public ResponseEntity resetServer(){
        playersQueuesCollection.clearAllQueues();
        tokenService.logoutAllUsers();
        gamePlaysService.removeAllGamePlays();
        lastGamePlaysResultsService.clear();
        logger.info("Server reset.");
        return UtilResponsesBuilder.buildOkResponse();
    }

    @DeleteMapping(ApiPaths.SERVER_URL + "/clearQueues")
    public ResponseEntity clearQueues(){
        playersQueuesCollection.clearAllQueues();
        logger.info("All queues cleared.");
        return UtilResponsesBuilder.buildOkResponse();
    }

    @DeleteMapping(ApiPaths.SERVER_URL + "/logoutAllUsers")
    public ResponseEntity logoutAllUsers(){
        tokenService.logoutAllUsers();
        logger.info("All users logged out.");
        return UtilResponsesBuilder.buildOkResponse();
    }

    @DeleteMapping(ApiPaths.SERVER_URL + "/removeAllGamePlays")
    public ResponseEntity removeAllGamePlays(){
        gamePlaysService.removeAllGamePlays();
        logger.info("All game plays removed.");
        return UtilResponsesBuilder.buildOkResponse();
    }

    @DeleteMapping(ApiPaths.SERVER_URL + "/removeAllGamePlaysResults")
    public ResponseEntity removeAllGamePlaysResults(){
        lastGamePlaysResultsService.clear();
        logger.info("All game plays results removed.");
        return UtilResponsesBuilder.buildOkResponse();
    }


}
