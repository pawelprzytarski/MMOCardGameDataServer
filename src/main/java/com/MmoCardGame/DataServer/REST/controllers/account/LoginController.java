package com.MmoCardGame.DataServer.REST.controllers.account;

import com.MmoCardGame.DataServer.REST.jsonMessages.AuthToken;
import com.MmoCardGame.DataServer.REST.responses.ResponseMessages;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.REST.utilConstants.HeaderNames;
import com.MmoCardGame.DataServer.security.tokenAuthentication.TokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

//In WebSecurityContext access to this controller is allowed only for authenticated users.
@RestController
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

    @Autowired
    private TokenService tokenService;

    @PostMapping(ApiPaths.LOGGED_USERS_URL)
    public AuthToken authenticate(
            HttpServletRequest request,
            HttpServletResponse response) {
        //AuthToken is returned in token header and in body of response for easy of use by client.
        String token = response.getHeader(HeaderNames.TOKEN_HEADER_NAME);
        AuthToken tokenResponse = new AuthToken(token);
        logger.info("Successfully logged in. Token response: {}", tokenResponse);
        return tokenResponse;
    }

    @DeleteMapping(ApiPaths.LOGGED_USERS_URL)
    public ResponseEntity logout(
            HttpServletRequest request,
            HttpServletResponse response){
        String token = request.getHeader(HeaderNames.TOKEN_HEADER_NAME);
        if(token == null){
            logger.error("Trying to logout without token.");
            return UtilResponsesBuilder.buildBadRequestResponseWithError(ResponseMessages.TOKEN_REQUIRED);
        }
        if(tokenService.contains(token)){
            tokenService.logoutUserByToken(token);
            logger.info("Successfully logged out. Token: {}", token);
            return UtilResponsesBuilder.buildOkResponseWithBody(ResponseMessages.LOGGED_OUT);
        }

        logger.error("Trying to logout with invalid token. Token: {}", token);
        return UtilResponsesBuilder.buildBadRequestResponseWithError(ResponseMessages.INVALID_TOKEN);
    }

}
