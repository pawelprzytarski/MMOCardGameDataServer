package com.MmoCardGame.DataServer.REST.controllers.account;

import com.MmoCardGame.DataServer.REST.jsonMessages.TokenFromEmailWithNewPassword;
import com.MmoCardGame.DataServer.REST.responses.ResponseMessages;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.users.resetPasswordTokens.ResetPasswordTokensService;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import com.MmoCardGame.DataServer.utilFunctionality.EmailSender;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
public class ResetPasswordController {

    private static final Logger logger = LoggerFactory.getLogger(ResetPasswordController.class);

    private final UsersService usersService;
    private final PasswordEncoder passwordEncoder;

    private ResetPasswordTokensService passwordTokensService;

    private EmailSender emailSender;

    @Autowired
    public ResetPasswordController(UsersService usersService, PasswordEncoder passwordEncoder, ResetPasswordTokensService passwordTokensService, EmailSender emailSender){
        this.usersService = usersService;
        this.passwordEncoder = passwordEncoder;
        this.passwordTokensService = passwordTokensService;
        this.emailSender = emailSender;
    }

    @PostMapping(ApiPaths.USERS_URL +"/{login}/password/reset")
    public ResponseEntity resetPasswordRequestWithLogin(@PathVariable String login){
        if(!usersService.isLoginUsed(login)){
            logger.warn("Trying to reset password of not existing user. Login: {}", login);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.USER_WITH_LOGIN_NOT_EXISTS);
        }
        User user = usersService.getUserByLogin(login);
        String token = passwordTokensService.generateNewToken();
        passwordTokensService.store(token, user.getLogin());

        sendEmail(token, user);

        logger.info("Reset password request successfully handled. Login: {}, Sent token: {}", login, token);

        return UtilResponsesBuilder.buildOkResponse();
    }

    private void sendEmail(String token, User user) {
        emailSender.sendMessage(user.getEmail()
                , "Resetowanie hasła"
                , "Prośba o  zresetowanie hasła została przyjęta. " +
                        "Wpisz podany kod do formularza w odpowiednie miejsce, " +
                        "żeby zresetować hasło: " + token);
    }

    @PutMapping(ApiPaths.USERS_URL +"/{login}/password/reset")
    public ResponseEntity resetPasswordWithToken(@RequestBody @Valid TokenFromEmailWithNewPassword tokenWithNewPassword,
                                                               BindingResult bindingResult,
                                                               @PathVariable String login){

        String token = tokenWithNewPassword.getToken();
        if(bindingResult.hasErrors()){
            logger.warn("Trying to reset password with token, with validation errors. Login: {}, token: {}, errors: {}",login, tokenWithNewPassword.getToken(), bindingResult.getAllErrors());
            return UtilResponsesBuilder.buildValidationErrorResponse(bindingResult);
        }

        if(!passwordTokensService.contains(token)){
            logger.warn("Trying to reset password with invalid token, which doesn't exist in tokens service. Login: {}, token: {}", login, tokenWithNewPassword.getToken());
            return UtilResponsesBuilder.buildBadRequestResponseWithError("Wrong token " + token);
        }

        String tokenLogin = passwordTokensService.retrieveLogin(token);
        if(!tokenLogin.equals(login)){
            logger.warn("Trying to reset password with token of other user. Login: {}, token: {}, actual for this token: {}", login, tokenWithNewPassword.getToken(), tokenLogin);
            return UtilResponsesBuilder.buildBadRequestResponseWithError("Wrong token " + token);
        }

        String newPassword = tokenWithNewPassword.getNewPassword();
        usersService.updatePasswordFor(passwordEncoder.encode(newPassword), login);
        passwordTokensService.remove(token);

        logger.info("Reset password with token request successfully handled. Login: {}, token: {}", login, token);
        return UtilResponsesBuilder.buildOkResponseWithBody(ResponseMessages.PASSWORD_RESET);
    }

}



