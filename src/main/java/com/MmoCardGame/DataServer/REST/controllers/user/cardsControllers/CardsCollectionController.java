package com.MmoCardGame.DataServer.REST.controllers.user.cardsControllers;

import com.MmoCardGame.DataServer.REST.jsonMessages.CardWithAmount;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CardsCollectionController {

    private static final Logger logger = LoggerFactory.getLogger(CardsCollectionController.class);

    private UsersService usersService;

    @Autowired
    public CardsCollectionController(UsersService usersService){
        this.usersService = usersService;
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}/cards")
    public ResponseEntity getAllUserCards(@PathVariable String login){
        User user = usersService.getUserByLogin(login);

        List<CardWithAmount> cardsCollection = user.getCardsWithAmounts();

        logger.info("Get all user cards: {}", cardsCollection);

        return UtilResponsesBuilder.buildOkResponseWithBody(cardsCollection);
    }

}
