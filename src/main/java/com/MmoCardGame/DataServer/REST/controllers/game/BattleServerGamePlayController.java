package com.MmoCardGame.DataServer.REST.controllers.game;

import com.MmoCardGame.DataServer.REST.jsonMessages.EndGamePlayInfo;
import com.MmoCardGame.DataServer.REST.responses.ResponseMessages;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.game.GamePlaysService;
import com.MmoCardGame.DataServer.users.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BattleServerGamePlayController {

    private static final Logger logger = LoggerFactory.getLogger(BattleServerGamePlayController.class);

    private GamePlaysService gamesService;
    private UsersService usersService;

    @Autowired
    public BattleServerGamePlayController(GamePlaysService gamesService, UsersService usersService){
        this.gamesService = gamesService;
        this.usersService = usersService;
    }

    @PutMapping(ApiPaths.GAME_PLAYS_URL)
    public ResponseEntity endGamePlayInfo(@RequestBody EndGamePlayInfo endGamePlayInfo){
        if(!usersService.isLoginUsed(endGamePlayInfo.getUserLogin())){
            logger.warn("Couldn't end game play by non existing user. Login: {}", endGamePlayInfo.getUserLogin());
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.USER_WITH_LOGIN_NOT_EXISTS);
        }
        if(!gamesService.userHasGame(endGamePlayInfo.getUserLogin())){
            logger.warn("Can't end game play while user doesn't wait for game play. Login: {}, Game play session id: {}",
                    endGamePlayInfo.getUserLogin(), endGamePlayInfo.getSessionId());
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.NO_GAME_PLAY_FOR_USER);
        }
        if(!gamesService.sessionExist(endGamePlayInfo.getSessionId())){
            logger.warn("Can't end game play because session with this id doesn't exist. Login: {}, Game play session id: {}",
                    endGamePlayInfo.getUserLogin(), endGamePlayInfo.getSessionId());
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.SESSION_NOT_EXIST);
        }
        gamesService.handleEndGamePlayInfo(endGamePlayInfo);
        logger.info("Info from battleserver received. Successfully ended game play by user. Login: {}, Game play session id: {}",
                endGamePlayInfo.getUserLogin(), endGamePlayInfo.getSessionId());
        return UtilResponsesBuilder.buildOkResponse();
    }
}
