package com.MmoCardGame.DataServer.REST.controllers.user.cardsControllers;

import com.MmoCardGame.DataServer.REST.responses.ResponseMessages;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class PacketsController {

    private static final Logger logger = LoggerFactory.getLogger(PacketsController.class);

    private UsersService usersService;

    @Autowired
    public PacketsController(UsersService usersService){
        this.usersService = usersService;
    }

    @GetMapping(ApiPaths.USERS_URL + "/{login}/packets/{packetType}")
    public ResponseEntity openPacket(@PathVariable String login,
                                        @PathVariable PacketsTypes packetType){
        User user = usersService.getUserByLogin(login);

        if(user.getNumberOfCardPackets().get(packetType) == 0){
            logger.warn("Trying to open packet while user has zero of them. Login: {}, Packet type: {}", login, packetType);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.NOT_FOUND, ResponseMessages.USER_HAVE_ZERO_PACKETS);
        }

        List<Card> openedCards = usersService.openUserPacket(login, packetType);

        if(openedCards.isEmpty()){
            logger.error("Packet opener returned zero cards. Login: {}, packet type: {}", login, packetType);
            return UtilResponsesBuilder.buildResponseWithMessage(HttpStatus.INTERNAL_SERVER_ERROR, ResponseMessages.PACKET_COULD_NOT_BE_OPENED);
        }

        logger.error("Packet opened. Login: {}, packet type: {}, opened cards: {}", login, packetType, openedCards);
        return UtilResponsesBuilder.buildOkResponseWithBody(openedCards);
    }

}
