package com.MmoCardGame.DataServer.exceptions;

public class NoSuchEmailPropertyNameException extends RuntimeException{
    public NoSuchEmailPropertyNameException(String s) {
        super(s);
    }
}
