package com.MmoCardGame.DataServer.exceptions;

public class NoSuchPersistencePropertyNameException extends RuntimeException{
    public NoSuchPersistencePropertyNameException(String s) {
        super(s);
    }
}
