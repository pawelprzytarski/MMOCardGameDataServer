package com.MmoCardGame.DataServer.exceptions;

public class NotEnoughPlayersInQueueToStartGamePlayException extends RuntimeException {
    public NotEnoughPlayersInQueueToStartGamePlayException(String s) {
        super(s);
    }
}
