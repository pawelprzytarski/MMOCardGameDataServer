package com.MmoCardGame.DataServer.exceptions;

public class UserIsAlreadyWaitingForGamePlay extends RuntimeException {
    public UserIsAlreadyWaitingForGamePlay(String login) {
        super(login);
    }
}
