package com.MmoCardGame.DataServer.exceptions;

public class NoSuchBattleServerPropertyNameException extends RuntimeException {
    public NoSuchBattleServerPropertyNameException(String s) {
        super(s);
    }
}
