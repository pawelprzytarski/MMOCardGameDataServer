package com.MmoCardGame.DataServer.exceptions;

import com.MmoCardGame.DataServer.game.data.GamePlayTypes;

public class NoSuchGamePlayTypeException extends RuntimeException {
    public NoSuchGamePlayTypeException(GamePlayTypes type) {
        super(type.toString());
    }
}
