package com.MmoCardGame.DataServer.exceptions;

import com.MmoCardGame.DataServer.game.data.GamePlayTypes;

public class QueueForThisGameTypeNotSupported extends RuntimeException {
    public QueueForThisGameTypeNotSupported(GamePlayTypes gameType) {
        super(gameType.toString());
    }
}
