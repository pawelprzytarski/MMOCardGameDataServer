package com.MmoCardGame.DataServer.exceptions;

public class NoSuchCardsPropertyNameException extends RuntimeException {
    public NoSuchCardsPropertyNameException(String s) {
        super(s);
    }
}
