package com.MmoCardGame.DataServer.exceptions;

import com.MmoCardGame.DataServer.shop.products.ProductNames;

public class ProductWithThatNameDoesNotExist extends RuntimeException {

    public ProductWithThatNameDoesNotExist(ProductNames name){
        super(name.name());
    }

}
