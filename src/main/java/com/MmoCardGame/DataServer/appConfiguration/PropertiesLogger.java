package com.MmoCardGame.DataServer.appConfiguration;

import com.MmoCardGame.DataServer.appConfiguration.cards.CardsProperties;
import com.MmoCardGame.DataServer.appConfiguration.email.EmailProperties;
import com.MmoCardGame.DataServer.appConfiguration.battleServer.BattleServerProperties;
import com.MmoCardGame.DataServer.appConfiguration.persistence.PersistenceProperties;
import com.MmoCardGame.DataServer.exceptions.NotPropertiesObjectException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;


@Service
public class PropertiesLogger {

    public static final String SEPARATOR = "========================================================";
    private static final Logger logger = LoggerFactory.getLogger(PropertiesLogger.class);

    public void log(Object propertiesObject){
        if(!isPropertiesObject(propertiesObject)){
            throw new NotPropertiesObjectException(propertiesObject.toString());
        }
        logger.info(SEPARATOR);
        logger.info("Loaded properties: {}", propertiesObject.getClass().getSimpleName());
        Field[] fields = propertiesObject.getClass().getDeclaredFields();
        for(Field field : fields){
            tryLogProperty(propertiesObject, field);
        }
        logger.info(SEPARATOR);
    }

    private void tryLogProperty(Object propertiesObject, Field field) {
        try {
            field.setAccessible(true);
            logger.info("{} : {}", field.getName(), field.get(propertiesObject));
        } catch (IllegalAccessException e) {
            logger.error(e.getMessage(), e);
        }
    }

    private boolean isPropertiesObject(Object object) {
        return  object instanceof CardsProperties ||
                object instanceof EmailProperties ||
                object instanceof BattleServerProperties ||
                object instanceof PersistenceProperties;
    }

}
