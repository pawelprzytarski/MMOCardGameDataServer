package com.MmoCardGame.DataServer.appConfiguration.email;

import com.MmoCardGame.DataServer.exceptions.NoSuchEmailPropertyNameException;

public enum EmailPropertiesNames {
    USER,
    HOST,
    PASSWORD;

    public static String getEnvName(EmailPropertiesNames name) {
        switch(name){
            case USER:
                return "email_user";
            case HOST:
                return "email_host";
            case PASSWORD:
                return "email_password";
        }

        throw new NoSuchEmailPropertyNameException(name.toString());
    }

    public static String getInFileName(EmailPropertiesNames name) {
        switch(name){
            case USER:
                return "mail.user";
            case HOST:
                return "mail.host";
            case PASSWORD:
                return "mail.password";
        }

        throw new NoSuchEmailPropertyNameException(name.toString());
    }

}
