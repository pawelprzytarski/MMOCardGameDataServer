package com.MmoCardGame.DataServer.appConfiguration.email;

import com.MmoCardGame.DataServer.appConfiguration.PropertyLoaderHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class EmailPropertiesLoader {


    private PropertyLoaderHelper propertyLoaderHelper;

    @Autowired
    public EmailPropertiesLoader(PropertyLoaderHelper propertyLoaderHelper){
        this.propertyLoaderHelper = propertyLoaderHelper;
    }

    @Bean
    public EmailProperties emailProperties(){

        EmailProperties emailProperties = new EmailProperties();

        String host = propertyLoaderHelper.loadStringProperty(
                DefaultEmailProperties.HOST,
                EmailPropertiesNames.getEnvName(EmailPropertiesNames.HOST),
                EmailPropertiesNames.getInFileName(EmailPropertiesNames.HOST));
        emailProperties.setHost(host);

        String password = propertyLoaderHelper.loadStringProperty(
                "",
                EmailPropertiesNames.getEnvName(EmailPropertiesNames.PASSWORD),
                EmailPropertiesNames.getInFileName(EmailPropertiesNames.PASSWORD));
        emailProperties.setPassword(password);

        String user = propertyLoaderHelper.loadStringProperty(
                DefaultEmailProperties.USER,
                EmailPropertiesNames.getEnvName(EmailPropertiesNames.USER),
                EmailPropertiesNames.getInFileName(EmailPropertiesNames.USER));
        emailProperties.setUser(user);

        return emailProperties;
    }

}
