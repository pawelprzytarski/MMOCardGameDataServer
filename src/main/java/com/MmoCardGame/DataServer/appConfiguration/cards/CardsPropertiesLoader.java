package com.MmoCardGame.DataServer.appConfiguration.cards;

import com.MmoCardGame.DataServer.appConfiguration.PropertyLoaderHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class CardsPropertiesLoader {


    private PropertyLoaderHelper propertyLoaderHelper;

    @Autowired
    public CardsPropertiesLoader(PropertyLoaderHelper propertyLoaderHelper){
        this.propertyLoaderHelper = propertyLoaderHelper;
    }

    @Bean
    public CardsProperties cardsProperties(){

        CardsProperties cardsProperties = new CardsProperties();

        Integer minCardsInDeck = propertyLoaderHelper.loadIntegerProperty(
                DefaultCardsProperties.MIN_CARDS_IN_DECK,
                CardsPropertiesNames.getEnvName(CardsPropertiesNames.MIN_CARDS_IN_DECK));
        cardsProperties.setMinCardsInDeck(minCardsInDeck);

        Integer minLinkCardsInDeck = propertyLoaderHelper.loadIntegerProperty(
                DefaultCardsProperties.MIN_LINK_CARDS_IN_DECK,
                CardsPropertiesNames.getEnvName(CardsPropertiesNames.MIN_LINK_CARDS_IN_DECK));
        cardsProperties.setMinLinkCardsInDeck(minLinkCardsInDeck);

        Integer cardsInSmallPacket = propertyLoaderHelper.loadIntegerProperty(
                DefaultCardsProperties.NUMBER_OF_CARDS_IN_SMALL_PACKET,
                CardsPropertiesNames.getEnvName(CardsPropertiesNames.NUMBER_OF_CARDS_IN_SMALL_PACKET));
        cardsProperties.setNumberOfCardsInSmallPacket(cardsInSmallPacket);

        Integer cardsInMediumPacket = propertyLoaderHelper.loadIntegerProperty(
                DefaultCardsProperties.NUMBER_OF_CARDS_IN_MEDIUM_PACKET,
                CardsPropertiesNames.getEnvName(CardsPropertiesNames.NUMBER_OF_CARDS_IN_MEDIUM_PACKET));
        cardsProperties.setNumberOfCardsInMediumPacket(cardsInMediumPacket);

        Integer cardsInBigPacket = propertyLoaderHelper.loadIntegerProperty(
                DefaultCardsProperties.NUMBER_OF_CARDS_IN_BIG_PACKET,
                CardsPropertiesNames.getEnvName(CardsPropertiesNames.NUMBER_OF_CARDS_IN_BIG_PACKET));
        cardsProperties.setNumberOfCardsInBigPacket(cardsInBigPacket);

        return cardsProperties;
    }

}