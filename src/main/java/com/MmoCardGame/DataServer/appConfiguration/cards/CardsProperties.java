package com.MmoCardGame.DataServer.appConfiguration.cards;

import lombok.Data;

@Data
public class CardsProperties {

    private int numberOfCardsInSmallPacket;
    private int numberOfCardsInMediumPacket;
    private int numberOfCardsInBigPacket;

    private int minCardsInDeck;
    private int minLinkCardsInDeck;

}
