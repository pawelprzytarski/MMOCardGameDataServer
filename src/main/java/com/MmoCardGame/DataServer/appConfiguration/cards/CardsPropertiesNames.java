package com.MmoCardGame.DataServer.appConfiguration.cards;

import com.MmoCardGame.DataServer.exceptions.NoSuchCardsPropertyNameException;

public enum CardsPropertiesNames {
    NUMBER_OF_CARDS_IN_SMALL_PACKET,
    NUMBER_OF_CARDS_IN_MEDIUM_PACKET,
    NUMBER_OF_CARDS_IN_BIG_PACKET,
    MIN_CARDS_IN_DECK,
    MIN_LINK_CARDS_IN_DECK;

    public static String getEnvName(CardsPropertiesNames name) {
        switch(name){
            case NUMBER_OF_CARDS_IN_SMALL_PACKET:
                return "cards_numberOfCardsInSmallPacket";
            case NUMBER_OF_CARDS_IN_MEDIUM_PACKET:
                return "cards_numberOfCardsInMediumPacket";
            case NUMBER_OF_CARDS_IN_BIG_PACKET:
                return "cards_numberOfCardsInBigPacket";
            case MIN_CARDS_IN_DECK:
                return "cards_minCardsInDeck";
            case MIN_LINK_CARDS_IN_DECK:
                return "cards_minLinkCardsInDeck";
        }
        throw new NoSuchCardsPropertyNameException(name.toString());
    }
}
