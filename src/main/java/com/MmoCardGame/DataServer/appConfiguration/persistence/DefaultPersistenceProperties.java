package com.MmoCardGame.DataServer.appConfiguration.persistence;

import lombok.Data;

@Data
public class DefaultPersistenceProperties {

    public static final String USER = "postgres";
    public static final String CONNECTION_STRING = "jdbc:localhost://localhost:5432/dataserverDB";
    public static final String AUTO_POLICY = "create-drop";

}
