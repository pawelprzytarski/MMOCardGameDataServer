package com.MmoCardGame.DataServer.appConfiguration.battleServer;

import com.MmoCardGame.DataServer.exceptions.NoSuchBattleServerPropertyNameException;

public enum BattleServerPropertiesNames{
     USE_MOCKED_BATTLE_SERVER,
     BATTLE_SERVER_PORT,
     BATTLE_SERVER_URL,
     BATTLE_SERVER_TOKEN;

    public static String getEnvName(BattleServerPropertiesNames name) {
        switch(name){
            case USE_MOCKED_BATTLE_SERVER:
                return "battleserver_useMocked";
            case BATTLE_SERVER_PORT:
                return "battleserver_port";
            case BATTLE_SERVER_URL:
                return "battleserver_url";
            case BATTLE_SERVER_TOKEN:
                return "battleserver_token";
        }

        throw new NoSuchBattleServerPropertyNameException(name.toString());
    }

    public static String getInFileName(BattleServerPropertiesNames name) {
        switch(name){
            case USE_MOCKED_BATTLE_SERVER:
                return "battleserver.useMocked";
            case BATTLE_SERVER_PORT:
                return "battleserver.port";
            case BATTLE_SERVER_URL:
                return "battleserver.url";
            case BATTLE_SERVER_TOKEN:
                return "battleserver.token";
        }

        throw new NoSuchBattleServerPropertyNameException(name.toString());
    }
}
