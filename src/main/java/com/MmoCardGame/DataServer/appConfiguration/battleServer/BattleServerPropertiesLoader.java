package com.MmoCardGame.DataServer.appConfiguration.battleServer;

import com.MmoCardGame.DataServer.appConfiguration.PropertyLoaderHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BattleServerPropertiesLoader {

    private PropertyLoaderHelper propertyLoaderHelper;

    @Autowired
    public BattleServerPropertiesLoader(PropertyLoaderHelper propertyLoaderHelper){
        this.propertyLoaderHelper = propertyLoaderHelper;
    }

    @Bean
    public BattleServerProperties generalAppProperties(){

        BattleServerProperties generalAppProperties = new BattleServerProperties();

        String token = propertyLoaderHelper.loadStringProperty(
                DefaultBattleServerProperties.BATTLE_SERVER_TOKEN,
                BattleServerPropertiesNames.getEnvName(BattleServerPropertiesNames.BATTLE_SERVER_TOKEN),
                BattleServerPropertiesNames.getInFileName(BattleServerPropertiesNames.BATTLE_SERVER_TOKEN));
        generalAppProperties.setBattleServerToken(token);

        Integer port = propertyLoaderHelper.loadIntegerProperty(
                DefaultBattleServerProperties.BATTLE_SERVER_PORT,
                BattleServerPropertiesNames.getEnvName(BattleServerPropertiesNames.BATTLE_SERVER_PORT),
                BattleServerPropertiesNames.getInFileName(BattleServerPropertiesNames.BATTLE_SERVER_PORT));
        generalAppProperties.setBattleServerPort(port);

        String url = propertyLoaderHelper.loadStringProperty(
                DefaultBattleServerProperties.BATTLE_SERVER_URL,
                BattleServerPropertiesNames.getEnvName(BattleServerPropertiesNames.BATTLE_SERVER_URL),
                BattleServerPropertiesNames.getInFileName(BattleServerPropertiesNames.BATTLE_SERVER_URL));
        generalAppProperties.setBattleServerUrl(url);

        Boolean useMocked = propertyLoaderHelper.loadBooleanProperty(
                DefaultBattleServerProperties.USE_MOCKED_BATTLE_SERVER,
                BattleServerPropertiesNames.getEnvName(BattleServerPropertiesNames.USE_MOCKED_BATTLE_SERVER),
                BattleServerPropertiesNames.getInFileName(BattleServerPropertiesNames.USE_MOCKED_BATTLE_SERVER));
        generalAppProperties.setUseMockedBattleServer(useMocked);

        return generalAppProperties;
    }

}
