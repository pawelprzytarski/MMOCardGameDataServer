package com.MmoCardGame.DataServer.appConfiguration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class PropertyLoaderHelper {

    private Environment environment;

    @Autowired
    public PropertyLoaderHelper(Environment environment){
        this.environment = environment;
    }

    public String loadStringProperty(String defaultValue, String envName, String propertyNameFile){
        String fromFileOrDefault = environment.getProperty(propertyNameFile, String.class, defaultValue);
        return environment.getProperty(envName, String.class, fromFileOrDefault);
    }

    public String loadStringProperty(String defaultValue, String envName){
        return environment.getProperty(envName, String.class, defaultValue);
    }

    public Integer loadIntegerProperty(Integer defaultValue, String envName, String propertyNameFile){
        Integer fromFileOrDefault = environment.getProperty(propertyNameFile, Integer.class, defaultValue);
        return environment.getProperty(envName, Integer.class, fromFileOrDefault);
    }

    public Integer loadIntegerProperty(Integer defaultValue, String envName){
        return environment.getProperty(envName, Integer.class, defaultValue);
    }

    public Boolean loadBooleanProperty(Boolean defaultValue, String envName, String propertyNameFile){
        Boolean fromFileOrDefault = environment.getProperty(propertyNameFile, Boolean.class, defaultValue);
        return environment.getProperty(envName, Boolean.class, fromFileOrDefault);
    }

    public Boolean loadBooleanProperty(Boolean defaultValue, String envName){
        return environment.getProperty(envName, Boolean.class, defaultValue);
    }

}
