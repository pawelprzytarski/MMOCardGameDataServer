package com.MmoCardGame.DataServer.appConfiguration.logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class LoggerConfigurationLoader {

    private static final String LOGGER_CONF_FILE_PATH = "src/main/resources/log4j2.properties";
    private static final String ENVIRONMENT_VARIABLES_PREFIX = "com_MmoCardGame_DataServer_";
    private static final String ROOT_LOG_LEVEL_PROP_NAME = "rootLogLevel";

    public static void load(){
        Set<String> propertiesNames = getPropertiesNames();
        Map<String, String> properties = loadProperties(propertiesNames);
        saveConfigurationFile(properties);
        System.out.println("Loaded logger properties:");
        for(Map.Entry<String, String> prop : properties.entrySet()){
            System.out.println(prop.getKey() + ": " + prop.getValue());
        }
    }

    private static void saveConfigurationFile(Map<String, String> properties) {
        String fileContent = getFileContent(properties);

        Path path = Paths.get(LOGGER_CONF_FILE_PATH);
        byte[] strToBytes = fileContent.getBytes();
        writeToFile(path, strToBytes);
    }

    private static void writeToFile(Path path, byte[] strToBytes) {
        try {
            Files.write(path, strToBytes);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Couldn't write to configuration file. Using default logger configuration.");
        }
    }

    private static Set<String> getPropertiesNames() {
        Set<String> propertiesNames = new HashSet<>();
        Map<String, String> environmentVariables = System.getenv();
        for (String envName : environmentVariables.keySet()) {
            if(envName.contains(ENVIRONMENT_VARIABLES_PREFIX)){
                propertiesNames.add(envName);
            }
        }
        addDefaultPropertiesNames(propertiesNames);
        return propertiesNames;
    }

    private static void addDefaultPropertiesNames(Set<String> propertiesNames) {
        propertiesNames.add(ROOT_LOG_LEVEL_PROP_NAME);
        propertiesNames.add("com_MmoCardGame_DataServer_game_playersQueues");
        propertiesNames.add("com_MmoCardGame_DataServer_REST_controllers");
        propertiesNames.add("com_MmoCardGame_DataServer_game_battleServerClient");
        propertiesNames.add("com_MmoCardGame_DataServer_game");
        propertiesNames.add("com_MmoCardGame_DataServer_users_validation");
        propertiesNames.add("com_MmoCardGame_DataServer_appConfiguration");
    }

    private static Map<String, String> loadProperties(Set<String> propertiesNames) {
        Map<String, String> loggerProperties = new HashMap<>();

        propertiesNames.forEach(name -> loadProperty(loggerProperties, name));

        return loggerProperties;
    }

    private static void loadProperty(Map<String, String> loggerProperties, String propertyName) {
        String logLevel = System.getenv(propertyName);
        if(logLevel != null) {
            loggerProperties.put(propertyName, logLevel);
        }
        else if(propertyName.equals(ROOT_LOG_LEVEL_PROP_NAME)) {
            loggerProperties.put(propertyName, "warn");
        } else{
            loggerProperties.put(propertyName, "info");
        }
    }

    private static String getFileContent(Map<String, String> properties){
        StringBuilder content = getContentHeader();
        appendRootLoggerConf(properties.get(ROOT_LOG_LEVEL_PROP_NAME), content);
        appendOtherLoggersConf(properties, content);

        return content.toString();
    }

    private static StringBuilder getContentHeader() {
        return new StringBuilder(
                "name=PropertiesConfig\n" +
                        "appenders = console\n" +
                        "\n" +
                        "appender.console.type = Console\n" +
                        "appender.console.name = STDOUT\n" +
                        "appender.console.layout.type = PatternLayout\n" +
                        "appender.console.layout.pattern = [%-5level] %d{HH:mm:ss.SSS} [%t] [%c{1}] %msg%n\n" +
                        "\n");
    }

    private static void appendRootLoggerConf(String rootLogLevel, StringBuilder content) {
        content.append("rootLogger.level = ").append(rootLogLevel).append("\n");
        content.append("rootLogger.appenderRefs = stdout\n");
        content.append("rootLogger.appenderRef.stdout.ref = STDOUT\n");
        content.append("\n");
    }

    private static void appendOtherLoggersConf(Map<String, String> properties, StringBuilder content) {
        for(Map.Entry<String, String> prop : properties.entrySet()){
            if(!prop.getKey().equals(ROOT_LOG_LEVEL_PROP_NAME)){
                String propName = prop.getKey();
                content.append("logger.").append(propName).append(".name = ").append(propName.replace("_", ".")).append("\n");
                content.append("logger.").append(propName).append(".level = ").append(properties.get(propName)).append("\n");
                content.append("\n");
            }
        }
    }

}
