alter table USERS_CARDS disable trigger all;

insert into USERS values (1,
                          'user1@mail.com',
                          'user1',
                          1000,
                          '$2a$10$FbyBUpwzOSa2ADzBgzerAeQ42tEWALXuGV2VnvC3e9JuIi9muBEFG',
                          'ROLE_USER',
                          'user1-deck1');
insert into USERS_CARDS values (1, 1, 20);
insert into USERS_CARDS values (2, 1, 20);
insert into USERS_CARDS values (3, 1, 20);
insert into USERS_CARDS values (4, 1, 20);
insert into USERS_CARDS values (5, 1, 20);
insert into USERS_CARDS values (6, 1, 20);
insert into USERS_CARDS values (7, 1, 20);
insert into USERS_CARDS values (8, 1, 20);
insert into USERS_CARDS values (9, 1, 20);
insert into USERS_CARDS values (10, 1, 20);
insert into USERS_CARDS values (11, 1, 20);
insert into USERS_CARDS values (12, 1, 20);
insert into USERS_CARDS values (13, 1, 20);
insert into USERS_CARDS values (19, 1, 20);
insert into USERS values (2,
                          'user2@mail.com',
                          'user2',
                          1000,
                          '$2a$10$FbyBUpwzOSa2ADzBgzerAeQ42tEWALXuGV2VnvC3e9JuIi9muBEFG',
                          'ROLE_USER',
                          'user2-deck1');
insert into USERS_CARDS values (1, 2, 20);
insert into USERS_CARDS values (2, 2, 20);
insert into USERS_CARDS values (3, 2, 20);
insert into USERS_CARDS values (4, 2, 20);
insert into USERS_CARDS values (5, 2, 20);
insert into USERS_CARDS values (6, 2, 20);
insert into USERS_CARDS values (7, 2, 20);
insert into USERS_CARDS values (8, 2, 20);
insert into USERS_CARDS values (9, 2, 20);
insert into USERS_CARDS values (10, 2, 20);
insert into USERS_CARDS values (11, 2, 20);
insert into USERS_CARDS values (12, 2, 20);
insert into USERS_CARDS values (13, 2, 20);
insert into USERS_CARDS values (19, 2, 20);
insert into USERS values (3,
                          'user3@mail.com',
                          'user3',
                          1000,
                          '$2a$10$FbyBUpwzOSa2ADzBgzerAeQ42tEWALXuGV2VnvC3e9JuIi9muBEFG',
                          'ROLE_USER',
                          'user3-deck1');
insert into USERS_CARDS values (1, 3, 20);
insert into USERS_CARDS values (2, 3, 20);
insert into USERS_CARDS values (3, 3, 20);
insert into USERS_CARDS values (4, 3, 20);
insert into USERS_CARDS values (5, 3, 20);
insert into USERS_CARDS values (6, 3, 20);
insert into USERS_CARDS values (7, 3, 20);
insert into USERS_CARDS values (8, 3, 20);
insert into USERS_CARDS values (9, 3, 20);
insert into USERS_CARDS values (10, 3, 20);
insert into USERS_CARDS values (11, 3, 20);
insert into USERS_CARDS values (12, 3, 20);
insert into USERS_CARDS values (13, 3, 20);
insert into USERS_CARDS values (19, 3, 20);
insert into USERS values (4,
                          'user4@mail.com',
                          'user4',
                          1000,
                          '$2a$10$FbyBUpwzOSa2ADzBgzerAeQ42tEWALXuGV2VnvC3e9JuIi9muBEFG',
                          'ROLE_USER',
                          'user4-deck1');
insert into USERS_CARDS values (1, 4, 20);
insert into USERS_CARDS values (2, 4, 20);
insert into USERS_CARDS values (3, 4, 20);
insert into USERS_CARDS values (4, 4, 20);
insert into USERS_CARDS values (5, 4, 20);
insert into USERS_CARDS values (6, 4, 20);
insert into USERS_CARDS values (7, 4, 20);
insert into USERS_CARDS values (8, 4, 20);
insert into USERS_CARDS values (9, 4, 20);
insert into USERS_CARDS values (10, 4, 20);
insert into USERS_CARDS values (11, 4, 20);
insert into USERS_CARDS values (12, 4, 20);
insert into USERS_CARDS values (13, 4, 20);
insert into USERS_CARDS values (19, 4, 20);
insert into USERS values (5,
                          'manyCards@mail.com',
                          'manyCards',
                          1000,
                          '$2a$10$FbyBUpwzOSa2ADzBgzerAeQ42tEWALXuGV2VnvC3e9JuIi9muBEFG',
                          'ROLE_USER',
                          'default');
insert into USERS_CARDS values (1, 5, 100);
insert into USERS_CARDS values (2, 5, 100);
insert into USERS_CARDS values (3, 5, 100);
insert into USERS_CARDS values (4, 5, 100);
insert into USERS_CARDS values (5, 5, 100);
insert into USERS_CARDS values (6, 5, 100);
insert into USERS_CARDS values (7, 5, 100);
insert into USERS_CARDS values (8, 5, 100);
insert into USERS values (6,
                          'fewCards@mail.com',
                          'fewCards',
                          1000,
                          '$2a$10$FbyBUpwzOSa2ADzBgzerAeQ42tEWALXuGV2VnvC3e9JuIi9muBEFG',
                          'ROLE_USER',
                          'default');
insert into USERS_CARDS values (1, 6, 2);
insert into USERS_CARDS values (2, 6, 2);
insert into USERS_CARDS values (3, 6, 2);
insert into USERS_CARDS values (4, 6, 2);
insert into USERS_CARDS values (5, 6, 2);
insert into USERS_CARDS values (6, 6, 2);
insert into USERS_CARDS values (7, 6, 2);
insert into USERS_CARDS values (8, 6, 2);
insert into USERS values (7,
                          'user7@mail.com',
                          'user7',
                          1000,
                          '$2a$10$FbyBUpwzOSa2ADzBgzerAeQ42tEWALXuGV2VnvC3e9JuIi9muBEFG',
                          'ROLE_USER',
                          'user7-deck1');
insert into USERS_CARDS values (1, 7, 30);
insert into USERS_CARDS values (5, 7, 30);
insert into USERS values (8,
                          'user8@mail.com',
                          'user8',
                          1000,
                          '$2a$10$FbyBUpwzOSa2ADzBgzerAeQ42tEWALXuGV2VnvC3e9JuIi9muBEFG',
                          'ROLE_USER',
                          'user8-deck1');
insert into USERS_CARDS values (1, 8, 30);
insert into USERS_CARDS values (5, 8, 30);

alter table USERS_CARDS enable trigger all;


alter table DECKS_CARDS disable trigger all;

insert into DECKS values (1, 'user1-deck1', 1);
insert into DECKS_CARDS values (1, 1, 20);
insert into DECKS_CARDS values (5, 1, 3);
insert into DECKS_CARDS values (6, 1, 3);
insert into DECKS_CARDS values (7, 1, 3);
insert into DECKS_CARDS values (8, 1, 3);
insert into DECKS_CARDS values (9, 1, 3);
insert into DECKS_CARDS values (10, 1, 3);
insert into DECKS_CARDS values (11, 1, 3);
insert into DECKS_CARDS values (12, 1, 3);
insert into DECKS_CARDS values (13, 1, 3);
insert into DECKS_CARDS values (19, 1, 3);
insert into DECKS values (2, 'user2-deck1', 2);
insert into DECKS_CARDS values (1, 2, 20);
insert into DECKS_CARDS values (5, 2, 3);
insert into DECKS_CARDS values (6, 2, 3);
insert into DECKS_CARDS values (7, 2, 3);
insert into DECKS_CARDS values (8, 2, 3);
insert into DECKS_CARDS values (9, 2, 3);
insert into DECKS_CARDS values (10, 2, 3);
insert into DECKS_CARDS values (11, 2, 3);
insert into DECKS_CARDS values (12, 2, 3);
insert into DECKS_CARDS values (13, 2, 3);
insert into DECKS_CARDS values (19, 2, 3);
insert into DECKS values (3, 'user3-deck1', 3);
insert into DECKS_CARDS values (1, 3, 20);
insert into DECKS_CARDS values (5, 3, 3);
insert into DECKS_CARDS values (6, 3, 3);
insert into DECKS_CARDS values (7, 3, 3);
insert into DECKS_CARDS values (8, 3, 3);
insert into DECKS_CARDS values (9, 3, 3);
insert into DECKS_CARDS values (10, 3, 3);
insert into DECKS_CARDS values (11, 3, 3);
insert into DECKS_CARDS values (12, 3, 3);
insert into DECKS_CARDS values (13, 3, 3);
insert into DECKS_CARDS values (19, 3, 3);
insert into DECKS values (4, 'user4-deck1', 4);
insert into DECKS_CARDS values (5, 4, 20);
insert into DECKS_CARDS values (6, 4, 3);
insert into DECKS_CARDS values (7, 4, 3);
insert into DECKS_CARDS values (8, 4, 3);
insert into DECKS_CARDS values (9, 4, 3);
insert into DECKS_CARDS values (10, 4, 3);
insert into DECKS_CARDS values (11, 4, 3);
insert into DECKS_CARDS values (12, 4, 3);
insert into DECKS_CARDS values (13, 4, 3);
insert into DECKS_CARDS values (19, 4, 3);
insert into DECKS values (7,
                          'user7-deck1', 7);
insert into DECKS_CARDS values (1, 7, 30);
insert into DECKS_CARDS values (5, 7, 30);
insert into DECKS values (8,
                          'user8-deck1', 8);
insert into DECKS_CARDS values (1, 8, 30);
insert into DECKS_CARDS values (5, 8, 30);

alter table DECKS_CARDS enable trigger all;

insert into USERS_PACKETS values (1, 10, 'SMALL_PACKET');
insert into USERS_PACKETS values (1, 10, 'MEDIUM_PACKET');
insert into USERS_PACKETS values (1, 10, 'BIG_PACKET');
insert into USERS_PACKETS values (2, 10, 'SMALL_PACKET');
insert into USERS_PACKETS values (2, 10, 'MEDIUM_PACKET');
insert into USERS_PACKETS values (2, 10, 'BIG_PACKET');
insert into USERS_PACKETS values (3, 10, 'SMALL_PACKET');
insert into USERS_PACKETS values (3, 10, 'MEDIUM_PACKET');
insert into USERS_PACKETS values (3, 10, 'BIG_PACKET');
insert into USERS_PACKETS values (4, 10, 'SMALL_PACKET');
insert into USERS_PACKETS values (4, 10, 'MEDIUM_PACKET');
insert into USERS_PACKETS values (4, 10, 'BIG_PACKET');
insert into USERS_PACKETS values (5, 10, 'SMALL_PACKET');
insert into USERS_PACKETS values (5, 10, 'MEDIUM_PACKET');
insert into USERS_PACKETS values (5, 10, 'BIG_PACKET');
insert into USERS_PACKETS values (6, 10, 'SMALL_PACKET');
insert into USERS_PACKETS values (6, 10, 'MEDIUM_PACKET');
insert into USERS_PACKETS values (6, 10, 'BIG_PACKET');