package com.MmoCardGame.DataServer.cards.cardsEntities;

import com.MmoCardGame.DataServer.users.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class CardTest {

    public ObjectMapper mapper;

    @Before
    public void init(){
        mapper = new ObjectMapper();
    }

    @Test
    public void serializeCard_cardShouldBeSerialized() throws JsonProcessingException {
        Card card = new Card();
        User user = new User();
        UserCard userCard = new UserCard(user, card);
        user.getCards().add(userCard);
        card.getUsers().add(userCard);

        String cardJson = mapper.writeValueAsString(card);
        System.out.println(cardJson);
        assertTrue(true);
    }
}