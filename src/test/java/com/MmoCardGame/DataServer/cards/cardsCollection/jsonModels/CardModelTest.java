package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels;

import com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels.enums.CardModelSubtype;
import com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels.enums.CardModelType;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class CardModelTest {

    private ObjectMapper mapper;
    private ActivatedAbilityModel activatedAbilityModel;
    private CostModel costModel;
    private ActivatedAbilityCostModel activatedAbilityCostModel;
    private StatisticsModel statisticsModel;

    @Before
    public void init() {
        mapper = new ObjectMapper();
        activatedAbilityModel = new ActivatedAbilityModel();
        activatedAbilityCostModel = new ActivatedAbilityCostModel();
        activatedAbilityCostModel.setNeutral(0);
        activatedAbilityCostModel.setPsychotronnics(1);
        activatedAbilityCostModel.setRaiders(2);
        activatedAbilityCostModel.setScanners(3);
        activatedAbilityCostModel.setWhiteHats(4);
        activatedAbilityCostModel.setOverload(5);
        activatedAbilityModel.setCost(activatedAbilityCostModel);
        activatedAbilityModel.setDescription("description");
        activatedAbilityModel.setId(1);
        costModel = new CostModel();
        costModel.setNeutral(0);
        costModel.setPsychotronnics(1);
        costModel.setRaiders(2);
        costModel.setScanners(3);
        costModel.setWhiteHats(4);
        statisticsModel = new StatisticsModel();
        statisticsModel.setDefence(20);
        statisticsModel.setAttack(10);
    }

    @Test
    public void serializeCardModelWithAllAttributes_cardModelShouldBeSerialized() throws JsonProcessingException {
        CardModel cardModel = new CardModel();
        cardModel.setId(1);
        List<Integer> abilities = new ArrayList<>();
        abilities.add(1);
        abilities.add(2);
        abilities.add(3);
        cardModel.setAbilities(abilities);
        ArrayList<ActivatedAbilityModel> activatedAbilities = new ArrayList<>();
        activatedAbilities.add(activatedAbilityModel);
        cardModel.setActivatedAbilities(activatedAbilities);
        cardModel.setDescription("description");
        cardModel.setName("name");
        cardModel.setCardClass("class");
        cardModel.setSubclass("subclass");
        cardModel.setType(CardModelType.Link);
        cardModel.setSubtype(CardModelSubtype.Basic);
        cardModel.setCost(costModel);
        cardModel.setStatistics(statisticsModel);

        String cardModelString = mapper.writeValueAsString(cardModel);
        System.out.println(cardModelString);
        String expected = "{\"id\":1," +
                "\"activatedAbilities\":" +
                "[{\"id\":1," +
                "\"description\":\"description\"," +
                "\"cost\":{" +
                "\"neutral\":0," +
                "\"raiders\":2," +
                "\"scanners\":3," +
                "\"whiteHats\":4," +
                "\"psychotronnics\":1," +
                "\"overload\":5}}]," +
                "\"abilities\":[1,2,3]," +
                "\"description\":\"description\"," +
                "\"name\":\"name\"," +
                "\"type\":\"Link\"," +
                "\"subtype\":\"Basic\"," +
                "\"subclass\":\"subclass\"," +
                "\"cost\":{" +
                "\"neutral\":0," +
                "\"raiders\":2," +
                "\"scanners\":3," +
                "\"whiteHats\":4," +
                "\"psychotronnics\":1}," +
                "\"statistics\":{" +
                "\"attack\":10," +
                "\"defence\":20}," +
                "\"class\":\"class\"}";
        assertEquals(expected, cardModelString);
    }

    @Test
    public void deserializeCardModel_cardModelShouldBeDeserialized() throws IOException {
        String cardModelString = "{\"id\":1," +
                "\"activatedAbilities\":" +
                "[{\"id\":1," +
                "\"description\":\"description\"," +
                "\"cost\":{" +
                "\"neutral\":0," +
                "\"raiders\":2," +
                "\"scanners\":3," +
                "\"whiteHats\":4," +
                "\"psychotronnics\":1," +
                "\"overload\":5}}]," +
                "\"abilities\":[1,2,3]," +
                "\"description\":\"description\"," +
                "\"name\":\"name\"," +
                "\"type\":\"Link\"," +
                "\"subtype\":\"Basic\"," +
                "\"subclass\":\"subclass\"," +
                "\"cost\":{" +
                "\"neutral\":0," +
                "\"raiders\":2," +
                "\"scanners\":3," +
                "\"whiteHats\":4," +
                "\"psychotronnics\":1}," +
                "\"statistics\":{" +
                "\"attack\":10," +
                "\"defence\":20}," +
                "\"class\":\"class\"}";

        CardModel cardModel = mapper.readValue(cardModelString, CardModel.class);
        System.out.println(cardModel);

        CardModel expected = new CardModel();
        expected.setId(1);
        List<Integer> abilities = new ArrayList<>();
        abilities.add(1);
        abilities.add(2);
        abilities.add(3);
        expected.setAbilities(abilities);
        ArrayList<ActivatedAbilityModel> activatedAbilities = new ArrayList<>();
        activatedAbilities.add(activatedAbilityModel);
        expected.setActivatedAbilities(activatedAbilities);
        expected.setDescription("description");
        expected.setName("name");
        expected.setCardClass("class");
        expected.setSubclass("subclass");
        expected.setType(CardModelType.Link);
        expected.setSubtype(CardModelSubtype.Basic);
        expected.setCost(costModel);
        expected.setStatistics(statisticsModel);
        assertEquals(expected, cardModel);
    }

    @Test
    public void deserializeCardModel_withoutTypesAndClasses_cardModelShouldBeDeserialized() throws IOException {
        String cardModelString = "{\"id\":1," +
                "\"activatedAbilities\":" +
                "[{\"id\":1," +
                "\"description\":\"description\"," +
                "\"cost\":{" +
                "\"neutral\":0," +
                "\"raiders\":2," +
                "\"scanners\":3," +
                "\"whiteHats\":4," +
                "\"psychotronnics\":1," +
                "\"overload\":5}}]," +
                "\"abilities\":[1,2,3]," +
                "\"description\":\"description\"," +
                "\"name\":\"name\"," +
                "\"cost\":{" +
                "\"neutral\":0," +
                "\"raiders\":2," +
                "\"scanners\":3," +
                "\"whiteHats\":4," +
                "\"psychotronnics\":1}," +
                "\"statistics\":{" +
                "\"attack\":10," +
                "\"defence\":20}}";

        CardModel cardModel = mapper.readValue(cardModelString, CardModel.class);
        System.out.println(cardModel);

        CardModel expected = new CardModel();
        expected.setId(1);
        List<Integer> abilities = new ArrayList<>();
        abilities.add(1);
        abilities.add(2);
        abilities.add(3);
        expected.setAbilities(abilities);
        ArrayList<ActivatedAbilityModel> activatedAbilities = new ArrayList<>();
        activatedAbilities.add(activatedAbilityModel);
        expected.setActivatedAbilities(activatedAbilities);
        expected.setDescription("description");
        expected.setName("name");
        expected.setCost(costModel);
        expected.setStatistics(statisticsModel);
        expected.setStatistics(statisticsModel);
        assertEquals(expected, cardModel);
    }

    @Test
    public void deserializeCardModel_withoutCost_cardModelShouldBeDeserialized() throws IOException {
        String cardModelString = "{\"id\":1," +
                "\"activatedAbilities\":" +
                "[{\"id\":1," +
                "\"description\":\"description\"," +
                "\"cost\":{" +
                "\"neutral\":0," +
                "\"raiders\":2," +
                "\"scanners\":3," +
                "\"whiteHats\":4," +
                "\"psychotronnics\":1," +
                "\"overload\":5}}]," +
                "\"abilities\":[1,2,3]," +
                "\"description\":\"description\"," +
                "\"name\":\"name\"," +
                "\"type\":\"Link\"," +
                "\"subtype\":\"Basic\"," +
                "\"subclass\":\"subclass\"," +
                "\"statistics\":{" +
                "\"attack\":10," +
                "\"defence\":20}," +
                "\"class\":\"class\"}";

        CardModel cardModel = mapper.readValue(cardModelString, CardModel.class);
        System.out.println(cardModel);

        CardModel expected = new CardModel();
        expected.setId(1);
        List<Integer> abilities = new ArrayList<>();
        abilities.add(1);
        abilities.add(2);
        abilities.add(3);
        expected.setAbilities(abilities);
        ArrayList<ActivatedAbilityModel> activatedAbilities = new ArrayList<>();
        activatedAbilities.add(activatedAbilityModel);
        expected.setActivatedAbilities(activatedAbilities);
        expected.setDescription("description");
        expected.setName("name");
        expected.setCardClass("class");
        expected.setSubclass("subclass");
        expected.setType(CardModelType.Link);
        expected.setSubtype(CardModelSubtype.Basic);
        expected.setStatistics(statisticsModel);
        assertEquals(expected, cardModel);
    }

    @Test
    public void deserializeCardModel_withoutStatistics_cardModelShouldBeDeserialized() throws IOException {
        String cardModelString = "{\"id\":1," +
                "\"activatedAbilities\":" +
                "[{\"id\":1," +
                "\"description\":\"description\"," +
                "\"cost\":{" +
                "\"neutral\":0," +
                "\"raiders\":2," +
                "\"scanners\":3," +
                "\"whiteHats\":4," +
                "\"psychotronnics\":1," +
                "\"overload\":5}}]," +
                "\"abilities\":[1,2,3]," +
                "\"description\":\"description\"," +
                "\"name\":\"name\"," +
                "\"type\":\"Link\"," +
                "\"subtype\":\"Basic\"," +
                "\"subclass\":\"subclass\"," +
                "\"cost\":{" +
                "\"neutral\":0," +
                "\"raiders\":2," +
                "\"scanners\":3," +
                "\"whiteHats\":4," +
                "\"psychotronnics\":1}," +
                "\"class\":\"class\"}";

        CardModel cardModel = mapper.readValue(cardModelString, CardModel.class);
        System.out.println(cardModel);

        CardModel expected = new CardModel();
        expected.setId(1);
        List<Integer> abilities = new ArrayList<>();
        abilities.add(1);
        abilities.add(2);
        abilities.add(3);
        expected.setAbilities(abilities);
        ArrayList<ActivatedAbilityModel> activatedAbilities = new ArrayList<>();
        activatedAbilities.add(activatedAbilityModel);
        expected.setActivatedAbilities(activatedAbilities);
        expected.setDescription("description");
        expected.setName("name");
        expected.setCost(costModel);
        expected.setCardClass("class");
        expected.setSubclass("subclass");
        expected.setType(CardModelType.Link);
        expected.setSubtype(CardModelSubtype.Basic);
        assertEquals(expected, cardModel);
    }

}