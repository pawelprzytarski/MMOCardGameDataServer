package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;

public class ActivatedAbilityModelTest {

    public ObjectMapper mapper;

    @Before
    public void init() {
        mapper = new ObjectMapper();
    }

    @Test
    public void serializeActivatedAbilityModel_activatedAbilityModelShouldBeSerialized() throws JsonProcessingException {
        ActivatedAbilityModel activatedAbilityModel = new ActivatedAbilityModel();
        ActivatedAbilityCostModel costModel = new ActivatedAbilityCostModel();
        costModel.setNeutral(0);
        costModel.setPsychotronnics(1);
        costModel.setRaiders(2);
        costModel.setScanners(3);
        costModel.setWhiteHats(4);
        costModel.setOverload(5);
        activatedAbilityModel.setCost(costModel);
        activatedAbilityModel.setDescription("description");
        activatedAbilityModel.setId(1);

        String activatedAbilityModelString = mapper.writeValueAsString(activatedAbilityModel);
        System.out.println(activatedAbilityModelString);
        String expected = "{\"id\":1,\"description\":\"description\",\"cost\":{\"neutral\":0,\"raiders\":2,\"scanners\":3,\"whiteHats\":4,\"psychotronnics\":1,\"overload\":5}}";
        assertEquals(expected, activatedAbilityModelString);
    }

    @Test
    public void deserializeActivatedAbilityModel_activatedAbilityModelShouldBeDeserialized() throws IOException {
        String activatedAbilityModelString = "{\"id\":1,\"description\":\"description\",\"cost\":{\"neutral\":0,\"raiders\":2,\"scanners\":3,\"whiteHats\":4,\"psychotronnics\":1,\"overload\":5}}";

        ActivatedAbilityModel activatedAbilityModel = mapper.readValue(activatedAbilityModelString, ActivatedAbilityModel.class);


        ActivatedAbilityModel expected = new ActivatedAbilityModel();
        ActivatedAbilityCostModel costModel = new ActivatedAbilityCostModel();
        costModel.setNeutral(0);
        costModel.setPsychotronnics(1);
        costModel.setRaiders(2);
        costModel.setScanners(3);
        costModel.setWhiteHats(4);
        costModel.setOverload(5);
        expected.setCost(costModel);
        expected.setDescription("description");
        expected.setId(1);

        System.out.println(expected);
        assertEquals(expected, activatedAbilityModel);
    }


}