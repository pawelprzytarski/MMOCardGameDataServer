package com.MmoCardGame.DataServer.cards.cardsCollection.jsonModels;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class JsonFileParserTest {

    public ObjectMapper mapper;

    private static String filePath = "testData/cards.json";

    @Before
    public void init() {
        mapper = new ObjectMapper();
    }

    @Test
    public void deserializeCostModel_costModelShouldBeSerialized() throws IOException {
        String fileContent = readFile(filePath);

        List<CardModel> cardModels = mapper.readValue(fileContent, new TypeReference<List<CardModel>>(){});

        System.out.println(cardModels);
        assertEquals(8, cardModels.size());
    }

    private String readFile(String filePath){
        List<String> lines= new ArrayList<>();
        try {
            lines = Files.readAllLines(Paths.get(filePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
        StringBuilder result = new StringBuilder();
        lines.forEach(result::append);
        return result.toString();
    }

}
