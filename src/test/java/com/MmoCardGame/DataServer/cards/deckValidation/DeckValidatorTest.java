package com.MmoCardGame.DataServer.cards.deckValidation;

import com.MmoCardGame.DataServer.cards.CardTypes;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsService;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import com.MmoCardGame.DataServer.cards.cardsEntities.Deck;
import com.MmoCardGame.DataServer.cards.cardsEntities.UserCard;
import com.MmoCardGame.DataServer.cards.configuration.CardsConsts;
import com.MmoCardGame.DataServer.cards.deckValidation.deckValidationResults.DeckValidationResult;
import com.MmoCardGame.DataServer.cards.deckValidation.deckValidationResults.DeckValidationResults;
import com.MmoCardGame.DataServer.users.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DeckValidatorTest {

    private Deck deck;

    private User user;

    private Card linkCard;

    private Card otherCard;

    private UserCard userCard;

    private UserCard userCard2;

    private CardsService cardsService;

    private static int numberOfUserCard = 10;

    @Before
    public void setup(){
        User user = Mockito.mock(User.class);
        Mockito.when(user.getLogin()).thenReturn("login");
        Mockito.when(user.getAmountOfCard(1L)).thenReturn(numberOfUserCard);
        int largeAmountOfCards = 100;
        Mockito.when(user.getAmountOfCard(2L)).thenReturn(largeAmountOfCards);
        linkCard = new Card(CardTypes.LINK);
        linkCard.setId(1L);
        userCard = new UserCard();
        userCard.setAmount(numberOfUserCard);
        userCard.setUser(user);
        userCard.setCard(linkCard);
        otherCard = new Card(CardTypes.OTHER);
        otherCard.setId(2L);
        userCard2 = new UserCard();
        userCard2.setAmount(largeAmountOfCards);
        userCard2.setUser(user);
        userCard2.setCard(otherCard);
        Set<UserCard> cardsSet = new HashSet<>();
        cardsSet.add(userCard);
        cardsSet.add(userCard2);
        Mockito.when(user.getCards()).thenReturn(cardsSet);

        deck = new Deck();
        deck.setUser(user);

        cardsService = Mockito.mock(CardsService.class);
    }

    @Test
    public void validateCardsAmount_withTooFewCards_shouldReturnError(){
        deck.addCard(linkCard);

        DeckValidator deckValidator = new DeckValidator(cardsService);

        List<DeckValidationResult> results = deckValidator.validateAmountOfCards(deck);

        assertEquals(results.get(0).getResult(), DeckValidationResults.NOT_ENOUGH_CARDS);
    }

    @Test
    public void validateCardsAmount_withOneTooFewCard_shouldReturnError(){
        for(int i = 0; i < CardsConsts.MIN_CARDS_IN_DECK - 1; i++) {
            deck.addCard(linkCard);
        }

        DeckValidator deckValidator = new DeckValidator(cardsService);

        List<DeckValidationResult> results = deckValidator.validateAmountOfCards(deck);

        assertEquals(results.get(0).getResult(), DeckValidationResults.NOT_ENOUGH_CARDS);
    }


    @Test
    public void validateCardsAmount_withEnoughCards_shouldReturnErrorsEmptyList(){
        for(int i = 0; i < CardsConsts.MIN_CARDS_IN_DECK; i++) {
            deck.addCard(linkCard);
        }

        DeckValidator deckValidator = new DeckValidator(cardsService);

        List<DeckValidationResult> results = deckValidator.validateAmountOfCards(deck);

        assertTrue(results.isEmpty());
    }

    @Test
    public void validateSpecificCardAmount_withOneTooFewCardOwnedByUser_shouldReturnError(){
        for(int i = 0; i < numberOfUserCard + 1; i++) {
            deck.addCard(linkCard);
        }

        DeckValidator deckValidator = new DeckValidator(cardsService);

        List<DeckValidationResult> results = deckValidator.validateAmountOfEachCard(deck);

        assertEquals(results.get(0).getResult(), DeckValidationResults.TOO_MANY_CARDS_OF_THIS_KIND);
    }


    @Test
    public void validateSpecificCardAmount_withEnoughCardsOwnedByUser_shouldReturnErrorsEmptyList(){
        for(int i = 0; i < numberOfUserCard; i++) {
            deck.addCard(linkCard);
        }

        DeckValidator deckValidator = new DeckValidator(cardsService);

        List<DeckValidationResult> results = deckValidator.validateAmountOfEachCard(deck);

        assertTrue(results.isEmpty());
    }

    @Test
    public void validateDeck_withEnoughCardsOwnedByUser_shouldReturnValidDeckResult(){
        for(int i = 0; i < numberOfUserCard; i++) {
            deck.addCard(linkCard);
        }
        for(int i = 0; i < CardsConsts.MIN_CARDS_IN_DECK; i++) {
            deck.addCard(otherCard);
        }

        DeckValidator deckValidator = new DeckValidator(cardsService);

        List<DeckValidationResult> results = deckValidator.validateDeck(deck);

        assertEquals(DeckValidationResults.DECK_IS_VALID, results.get(0).getResult());
    }


    @Test
    public void validateDeck_withNotEnoughCardsOwnedByUser_shouldReturnValidDeckResult(){
        for(int i = 0; i < numberOfUserCard+1; i++) {
            deck.addCard(linkCard);
        }
        for(int i = 0; i < CardsConsts.MIN_CARDS_IN_DECK; i++) {
            deck.addCard(otherCard);
        }

        DeckValidator deckValidator = new DeckValidator(cardsService);

        List<DeckValidationResult> results = deckValidator.validateDeck(deck);

        assertEquals(DeckValidationResults.TOO_MANY_CARDS_OF_THIS_KIND, results.get(0).getResult());
    }



    @Test
    public void validateDeck_withNotEnoughCards_shouldReturnValidDeckResult(){
        for(int i = 0; i < numberOfUserCard; i++) {
            deck.addCard(linkCard);
        }
        for(int i = 0; i < CardsConsts.MIN_CARDS_IN_DECK - 1 - numberOfUserCard; i++) {
            deck.addCard(otherCard);
        }

        DeckValidator deckValidator = new DeckValidator(cardsService);

        List<DeckValidationResult> results = deckValidator.validateDeck(deck);

        assertEquals(DeckValidationResults.NOT_ENOUGH_CARDS, results.get(0).getResult());
    }

    @Test
    public void validateDeck_withNotEnoughCardsAndWithNotEnoughCardsOwnedByUser_shouldReturnTwoValidDeckResults(){
        for(int i = 0; i < numberOfUserCard + 1; i++) {
            deck.addCard(linkCard);
        }
        for(int i = 0; i < CardsConsts.MIN_CARDS_IN_DECK - 2 - numberOfUserCard; i++) {
            deck.addCard(otherCard);
        }

        DeckValidator deckValidator = new DeckValidator(cardsService);

        List<DeckValidationResult> results = deckValidator.validateDeck(deck);

        assertEquals(2, results.size());
    }

    @Test
    public void validateDeck_withNotEnoughLinkCardsOwnedByUser_shouldReturnTwoValidDeckResults(){
        for(int i = 0; i < CardsConsts.MIN_LINK_CARDS - 1; i++) {
            deck.addCard(linkCard);
        }
        for(int i = 0; i < CardsConsts.MIN_CARDS_IN_DECK; i++) {
            deck.addCard(otherCard);
        }

        DeckValidator deckValidator = new DeckValidator(cardsService);

        List<DeckValidationResult> results = deckValidator.validateDeck(deck);

        assertEquals(1, results.size());
        assertEquals(DeckValidationResults.NOT_ENOUGH_LINK_CARDS, results.get(0).getResult());
    }
}