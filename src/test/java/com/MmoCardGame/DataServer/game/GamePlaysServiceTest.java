package com.MmoCardGame.DataServer.game;


import com.MmoCardGame.DataServer.exceptions.UserIsAlreadyWaitingForGamePlay;
import com.MmoCardGame.DataServer.game.battleServerClient.BattleServerClient;
import com.MmoCardGame.DataServer.game.data.GamePlayStartRequest;
import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import com.MmoCardGame.DataServer.game.playersQueues.*;
import com.MmoCardGame.DataServer.game.results.LastGamePlaysResultsService;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class GamePlaysServiceTest {

    private PlayersQueuesCollection playersQueuesCollection;
    private GamePlaysService gamePlaysService;
    private GamePlaysCollection gamePlaysCollection;
    private UsersService usersService;
    private LastGamePlaysResultsService lastGamePlaysResultsService;

    private BattleServerClient battleServerClient;

    private List<User> randomUsers;

    @Before
    public void setup(){
        randomUsers = new ArrayList<>();
        for(int i = 0; i < 10; i++){
            User mock = Mockito.mock(User.class);
            Mockito.when(mock.getLogin()).thenReturn("login"+i);
            randomUsers.add(mock);
        }
        playersQueuesCollection = new PlayersQueuesCollection(new TwoPlayersQueue(), new ThreePlayersQueue(), new FourPlayersQueue(), new TeamPlayersQueue());
        gamePlaysCollection = new GamePlaysCollection();
        usersService = new UsersService();
        battleServerClient = Mockito.mock(BattleServerClient.class);
        lastGamePlaysResultsService = Mockito.mock(LastGamePlaysResultsService.class);
        gamePlaysService = new GamePlaysService(gamePlaysCollection,
                playersQueuesCollection,
                battleServerClient,
                usersService,
                lastGamePlaysResultsService);
    }

    @Test
    public void dispatchingGamePlayStartRequest_toTwoPlayersGamePlay_shouldAddPlayerToTwoPlayersQueue(){
        GamePlayTypes gamePlayType = GamePlayTypes.TWO_PLAYERS;
        User user = randomUsers.get(0);
        GamePlayStartRequest gamePlayStartRequest = new GamePlayStartRequest(user, gamePlayType);

        gamePlaysService.dispatchGameStartRequest(gamePlayStartRequest);

        boolean result = playersQueuesCollection.getQueue(gamePlayType).contains(user);
        assertTrue(result);
    }

    @Test
    public void dispatchingGamePlayStartRequest_toThreePlayersGamePlay_shouldAddPlayerToThreePlayersQueue(){
        GamePlayTypes gamePlayType = GamePlayTypes.THREE_PLAYERS;
        User user = randomUsers.get(0);
        GamePlayStartRequest gamePlayStartRequest = new GamePlayStartRequest(user, gamePlayType);

        gamePlaysService.dispatchGameStartRequest(gamePlayStartRequest);

        boolean result = playersQueuesCollection.getQueue(gamePlayType).contains(user);
        assertTrue(result);
    }

    @Test
    public void dispatchingGamePlayStartRequest_toFourPlayersGamePlay_shouldAddPlayerToFourPlayersQueue(){
        GamePlayTypes gamePlayType = GamePlayTypes.FOUR_PLAYERS;
        User user = randomUsers.get(0);
        GamePlayStartRequest gamePlayStartRequest = new GamePlayStartRequest(user, gamePlayType);

        gamePlaysService.dispatchGameStartRequest(gamePlayStartRequest);

        boolean result = playersQueuesCollection.getQueue(gamePlayType).contains(user);
        assertTrue(result);
    }

    @Test
    public void dispatchingGamePlayStartRequest_toTeamGamePlay_shouldAddPlayerToTeamPlayersQueue(){
        GamePlayTypes gamePlayType = GamePlayTypes.TEAM_GAME;
        User user = randomUsers.get(0);
        GamePlayStartRequest gamePlayStartRequest = new GamePlayStartRequest(user, gamePlayType);

        gamePlaysService.dispatchGameStartRequest(gamePlayStartRequest);

        boolean result = playersQueuesCollection.getQueue(gamePlayType).contains(user);
        assertTrue(result);
    }

    @Test
    public void removingPlayerFromService_shouldRemoveHimFromQueuesCollection(){
        GamePlayTypes gamePlayType = GamePlayTypes.TEAM_GAME;
        User user = randomUsers.get(0);
        GamePlayStartRequest gamePlayStartRequest = new GamePlayStartRequest(user, gamePlayType);
        gamePlaysService.dispatchGameStartRequest(gamePlayStartRequest);

        gamePlaysService.removeWaitingPlayer(user);

        boolean result = playersQueuesCollection.isUserWaiting(user);
        assertFalse(result);
    }

    @Test(expected = UserIsAlreadyWaitingForGamePlay.class)
    public void dispatchingGamePlayStartRequest_withAlreadyWaitingPlayerToAnotherGamePlay_shouldThrowException(){
        GamePlayTypes gamePlayType = GamePlayTypes.TEAM_GAME;
        User user = randomUsers.get(0);
        GamePlayStartRequest gamePlayStartRequest = new GamePlayStartRequest(user, gamePlayType);
        GamePlayTypes gamePlayType2 = GamePlayTypes.FOUR_PLAYERS;
        GamePlayStartRequest gamePlayStartRequest2 = new GamePlayStartRequest(user, gamePlayType2);
        gamePlaysService.dispatchGameStartRequest(gamePlayStartRequest);

        gamePlaysService.dispatchGameStartRequest(gamePlayStartRequest2);

    }

    @Test
    public void startAllPossibleGames_withPlayersForTwoGames_shouldStartExactlyTwoGames(){
        for(int i = 0; i < 2; i++) {
            GamePlayTypes gamePlayType = GamePlayTypes.TWO_PLAYERS;
            User user = randomUsers.get(i);
            GamePlayStartRequest gamePlayStartRequest = new GamePlayStartRequest(user, gamePlayType);
            gamePlaysService.dispatchGameStartRequest(gamePlayStartRequest);
        }
        for(int i = 2; i < 6; i++) {
            GamePlayTypes gamePlayType = GamePlayTypes.THREE_PLAYERS;
            User user = randomUsers.get(i + 2);
            GamePlayStartRequest gamePlayStartRequest = new GamePlayStartRequest(user, gamePlayType);
            gamePlaysService.dispatchGameStartRequest(gamePlayStartRequest);
        }
        GamePlayTypes gamePlayType = GamePlayTypes.FOUR_PLAYERS;
        User user = randomUsers.get(8);
        GamePlayStartRequest gamePlayStartRequest = new GamePlayStartRequest(user, gamePlayType);
        gamePlaysService.dispatchGameStartRequest(gamePlayStartRequest);


        gamePlaysService.startAllPossibleGamePlays();

        assertEquals(2, gamePlaysService.getExistingGamePlays().size());
    }

}