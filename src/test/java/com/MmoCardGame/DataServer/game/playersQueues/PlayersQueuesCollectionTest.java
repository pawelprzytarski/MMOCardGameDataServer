package com.MmoCardGame.DataServer.game.playersQueues;

import com.MmoCardGame.DataServer.exceptions.NotEnoughPlayersInQueueToStartGamePlayException;
import com.MmoCardGame.DataServer.exceptions.UserIsAlreadyWaitingForGamePlay;
import com.MmoCardGame.DataServer.game.data.GamePlay;
import com.MmoCardGame.DataServer.game.data.GamePlayTypes;
import com.MmoCardGame.DataServer.users.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class PlayersQueuesCollectionTest {

    private PlayersQueuesCollection playersQueuesCollection;

    private List<User> randomUsers;

    @Before
    public void setup(){
        randomUsers = new ArrayList<>();
        for(int i = 0; i < 5; i++){
            User mock = Mockito.mock(User.class);
            randomUsers.add(mock);
            Mockito.when(mock.getLogin()).thenReturn("login" + i);
        }
        playersQueuesCollection = new PlayersQueuesCollection(new TwoPlayersQueue(), new ThreePlayersQueue(), new FourPlayersQueue(), new TeamPlayersQueue());
    }

    @Test
    public void retrievingQueue_forTwoPlayersGamePlay_shouldReturnInstanceOfTwoPlayersQueue(){
        GamePlayTypes gameType = GamePlayTypes.TWO_PLAYERS;

        PlayersQueue result = playersQueuesCollection.getQueue(gameType);

        assertTrue(result instanceof TwoPlayersQueue);
    }

    @Test
    public void retrievingQueue_forThreePlayersGamePlay_shouldReturnInstanceOfThreePlayersQueue(){
        GamePlayTypes gameType = GamePlayTypes.THREE_PLAYERS;

        PlayersQueue result = playersQueuesCollection.getQueue(gameType);

        assertTrue(result instanceof ThreePlayersQueue);
    }

    @Test
    public void retrievingQueue_forFourPlayersGamePlay_shouldReturnInstanceOfFourPlayersQueue(){
        GamePlayTypes gameType = GamePlayTypes.FOUR_PLAYERS;

        PlayersQueue result = playersQueuesCollection.getQueue(gameType);

        assertTrue(result instanceof FourPlayersQueue);
    }

    @Test
    public void retrievingQueue_forTeamGamePlay_shouldReturnInstanceOfTeamPlayersQueue(){
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;

        PlayersQueue result = playersQueuesCollection.getQueue(gameType);

        assertTrue(result instanceof TeamPlayersQueue);
    }

    @Test
    public void checkingIfUserIsWaiting_whileHeIsWaiting_shouldReturnTrue(){
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        playersQueuesCollection.getQueue(gameType).add(randomUsers.get(0));

        boolean result = playersQueuesCollection.isUserWaiting(randomUsers.get(0));

        assertTrue(result);
    }

    @Test
    public void canCreateGame_ofTeamGameTypeWithEnoughPlayers_shouldReturnTrue(){
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        for(int i = 0; i < 4; i++) {
            playersQueuesCollection.putToQueue(gameType, randomUsers.get(i));
        }

        boolean result = playersQueuesCollection.canCreateGame(gameType);

        assertTrue(result);
    }

    @Test
    public void canCreateGame_ofTeamGameTypeWithOneLackingPlayer_shouldReturnFalse(){
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        for(int i = 0; i < 3; i++) {
            playersQueuesCollection.putToQueue(gameType, randomUsers.get(i));
        }

        boolean result = playersQueuesCollection.canCreateGame(gameType);

        assertFalse(result);
    }

    @Test
    public void canCreateGame_withEmptyQueue_shouldReturnFalse(){
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;

        boolean result = playersQueuesCollection.canCreateGame(gameType);

        assertFalse(result);
    }

    @Test
    public void putToQueue_shouldPlaceUserInProperQueue(){
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        User user = randomUsers.get(0);

        playersQueuesCollection.putToQueue(gameType, user);

        boolean result = playersQueuesCollection.getQueue(gameType).contains(user);
        assertTrue(result);
    }

    @Test(expected = UserIsAlreadyWaitingForGamePlay.class)
    public void putToQueue_alreadyWaitingUser_shouldThrowException(){
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        GamePlayTypes gameType2 = GamePlayTypes.FOUR_PLAYERS;
        User user = randomUsers.get(0);

        playersQueuesCollection.putToQueue(gameType, user);
        playersQueuesCollection.putToQueue(gameType2, user);
    }

    @Test
    public void removeFromQueue_shouldRemoveUserFromCollection(){
        GamePlayTypes gameType = GamePlayTypes.FOUR_PLAYERS;
        User user = randomUsers.get(0);
        playersQueuesCollection.putToQueue(gameType, user);

        playersQueuesCollection.removeWaitingPlayer(user);

        boolean result = playersQueuesCollection.isUserWaiting(user);
        assertFalse(result);
    }

    @Test
    public void putToQueue_shouldIncreaseProperQueueSizeByOne(){
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        User user = randomUsers.get(0);
        int sizeBefore = playersQueuesCollection.getQueue(gameType).size();

        playersQueuesCollection.putToQueue(gameType, user);

        int size = playersQueuesCollection.getQueue(gameType).size();
        assertEquals(size, sizeBefore+1);
    }

    @Test(expected = NotEnoughPlayersInQueueToStartGamePlayException.class)
    public void createGame_withNotEnoughPlayersInQueue_shouldThrowException(){
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        for(int i = 0; i < 3; i++) {
            playersQueuesCollection.putToQueue(gameType, randomUsers.get(i));
        }

        playersQueuesCollection.createGame(gameType);
    }

    @Test
    public void createTeamGame_withEnoughPlayersInQueue_shouldReturnProperGamePlay() {
        GamePlayTypes gameType = GamePlayTypes.TEAM_GAME;
        for(int i = 0; i < 4; i++) {
            playersQueuesCollection.putToQueue(gameType, randomUsers.get(i));
        }

        GamePlay gamePlay = playersQueuesCollection.createGame(gameType);

        assertEquals(4, gamePlay.getPlayers().size());
        assertEquals(gameType, gamePlay.getGameType());
    }

    @Test
    public void createTwoPlayersGame_withEnoughPlayersInQueue_shouldReturnProperGamePlay()throws Exception {
        GamePlayTypes gameType = GamePlayTypes.TWO_PLAYERS;
        for(int i = 0; i < 2; i++) {
            playersQueuesCollection.putToQueue(gameType, randomUsers.get(i));
        }

        GamePlay gamePlay = playersQueuesCollection.createGame(gameType);

        assertEquals(2, gamePlay.getPlayers().size());
        assertEquals(gameType, gamePlay.getGameType());
    }

    @Test
    public void createThreePlayersGame_withEnoughPlayersInQueue_shouldReturnGamePlayWithProperPlayersFIFO()throws Exception {
        GamePlayTypes gameType = GamePlayTypes.THREE_PLAYERS;
        for(int i = 0; i < 4; i++) {
            playersQueuesCollection.putToQueue(gameType, randomUsers.get(i));
        }

        GamePlay gamePlay = playersQueuesCollection.createGame(gameType);

        assertEquals(3, gamePlay.getPlayers().size());
        assertEquals(randomUsers.get(0), gamePlay.getPlayers().get(randomUsers.get(0).getLogin()));
        assertEquals(randomUsers.get(1), gamePlay.getPlayers().get(randomUsers.get(1).getLogin()));
        assertEquals(randomUsers.get(2), gamePlay.getPlayers().get(randomUsers.get(2).getLogin()));
    }

}