package com.MmoCardGame.DataServer.users.resetPasswordTokens;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


@SpringBootTest
@RunWith(SpringRunner.class)
public class ResetPasswordTokensServiceTest {

    @Test
    public void retrievingStoredToken_shouldReturnProperLogin(){
        ResetPasswordTokensService tokensService = new ResetPasswordTokensService();
        String randomToken = "123456";
        String randomLogin = "qwe";
        tokensService.store(randomToken, randomLogin);

        String retrievedLogin = tokensService.retrieveLogin(randomToken);

        assertEquals(retrievedLogin, randomLogin);
    }

    @Test
    public void storingToShortToken_shouldNotAddTokenToCollection(){
        ResetPasswordTokensService tokensService = new ResetPasswordTokensService();
        String invalidToken = "12345";
        String randomLogin = "qwe";
        tokensService.store(invalidToken, randomLogin);

        boolean isTokenAdded = tokensService.contains(invalidToken);

        assertFalse(isTokenAdded);
    }

    @Test
    public void storingInvalidTokenWithLetters_shouldNotAddTokenToCollection(){
        ResetPasswordTokensService tokensService = new ResetPasswordTokensService();
        String invalidToken = "12qwe5";
        String randomLogin = "qwe";
        tokensService.store(invalidToken, randomLogin);

        boolean isTokenAdded = tokensService.contains(invalidToken);

        assertFalse(isTokenAdded);
    }

    @Test
    public void generateNewToken_shouldReturnValidToken(){
        ResetPasswordTokensService service = new ResetPasswordTokensService();
        String generatedToken = service.generateNewToken();
        String tokenRegexp = "[\\d]{6}";

        Pattern pattern = Pattern.compile(tokenRegexp);
        Matcher matcher = pattern.matcher(generatedToken);
        boolean isMatching = matcher.matches();

        assertTrue(isMatching);
    }

}