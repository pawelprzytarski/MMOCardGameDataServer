package com.MmoCardGame.DataServer.users.validation.fieldsMatching;

import com.MmoCardGame.DataServer.REST.jsonMessages.ChangeEmailData;
import com.MmoCardGame.DataServer.REST.jsonMessages.ChangePasswordData;
import com.MmoCardGame.DataServer.REST.jsonMessages.RegistrationData;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class FieldsMatchesValidatorTest {

    private FieldsMatchesValidator fieldsMatchesValidator;

    private RegistrationData registrationData;

    private ChangePasswordData changePasswordMessage;

    private ChangeEmailData changeEmailMessage;

    @Before
    public void init(){
        registrationData = new RegistrationData();
        changeEmailMessage = new ChangeEmailData();
        changePasswordMessage = new ChangePasswordData();
        fieldsMatchesValidator = new FieldsMatchesValidator();
    }

    @Test
    public void isValid_WithNotMatchingPasswordsInRegistrationData_shouldReturnFalse(){
        registrationData.setPassword("qwe");
        registrationData.setRepeatedPassword("asd");

        fieldsMatchesValidator.setFirstFieldName("password");
        fieldsMatchesValidator.setSecondFieldName("repeatedPassword");

        boolean validationResult = fieldsMatchesValidator.isValid(registrationData);

        assertFalse(validationResult);
    }

    @Test
    public void isValid_WithMatchingPasswordsInRegistrationData_shouldReturnTrue(){
        registrationData.setPassword("qwe");
        registrationData.setRepeatedPassword("qwe");

        fieldsMatchesValidator.setFirstFieldName("password");
        fieldsMatchesValidator.setSecondFieldName("repeatedPassword");

        boolean validationResult = fieldsMatchesValidator.isValid(registrationData);

        assertTrue(validationResult);
    }

    @Test
    public void isValid_WithNotMatchingPasswordsInChangePasswordMessage_shouldReturnFalse(){
        changePasswordMessage.setNewPassword("asd");
        changePasswordMessage.setRepeatedNewPassword("qwe");

        fieldsMatchesValidator.setFirstFieldName("newPassword");
        fieldsMatchesValidator.setSecondFieldName("repeatedNewPassword");

        boolean validationResult = fieldsMatchesValidator.isValid(changePasswordMessage);

        assertFalse(validationResult);
    }

    @Test
    public void isValid_WithMatchingPasswordsInChangePasswordMessage_shouldReturnTrue(){
        changePasswordMessage.setNewPassword("qwe");
        changePasswordMessage.setRepeatedNewPassword("qwe");

        fieldsMatchesValidator.setFirstFieldName("newPassword");
        fieldsMatchesValidator.setSecondFieldName("repeatedNewPassword");

        boolean validationResult = fieldsMatchesValidator.isValid(changePasswordMessage);

        assertTrue(validationResult);
    }

    @Test
    public void isValid_WithNotMatchingPasswordsInChangeEmailMessage_shouldReturnFalse(){
        changeEmailMessage.setNewEmail("asd@asd.asd");
        changeEmailMessage.setRepeatedEmail("qwe@qwe.qwe");

        fieldsMatchesValidator.setFirstFieldName("newEmail");
        fieldsMatchesValidator.setSecondFieldName("repeatedEmail");

        boolean validationResult = fieldsMatchesValidator.isValid(changeEmailMessage);

        assertFalse(validationResult);
    }

    @Test
    public void isValid_WithMatchingPasswordsInChangeEmailMessage_shouldReturnTrue(){
        changeEmailMessage.setNewEmail("asd@asd.asd");
        changeEmailMessage.setRepeatedEmail("asd@asd.asd");

        fieldsMatchesValidator.setFirstFieldName("newEmail");
        fieldsMatchesValidator.setSecondFieldName("repeatedEmail");

        boolean validationResult = fieldsMatchesValidator.isValid(changeEmailMessage);

        assertTrue(validationResult);
    }


}