package com.MmoCardGame.DataServer.users.validation.password;

import org.junit.Test;
import org.mockito.Mock;

import javax.validation.ConstraintValidatorContext;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class PasswordValidatorTest {

    @Mock
    private ConstraintValidatorContext constraintValidatorContextMock;

    @Test
    public void isValid_WithToShort6SignsPassword_shouldReturnFalse(){
        String invalidPassword = generatePassword(6);
        PasswordValidator PasswordValidator = new PasswordValidator();

        boolean validationResult = PasswordValidator.isValid(invalidPassword, constraintValidatorContextMock);

        assertFalse(validationResult);

    }

    private String generatePassword(int length) {
        StringBuilder passwordBuilder = new StringBuilder();
        for(int i = 0; i < length; i++){
            passwordBuilder.append("a");
        }
        return passwordBuilder.toString();
    }

    @Test
    public void isValid_WithToLong36SignsPassword_shouldReturnFalse(){
        String invalidPassword = generatePassword(36);
        PasswordValidator PasswordValidator = new PasswordValidator();

        boolean validationResult = PasswordValidator.isValid(invalidPassword, constraintValidatorContextMock);

        assertFalse(validationResult);
    }

    @Test
    public void isValid_WithPasswordWithWhitespaces_shouldReturnFalse(){
        String invalidPassword = "123q we#$%";
        PasswordValidator PasswordValidator = new PasswordValidator();

        boolean validationResult = PasswordValidator.isValid(invalidPassword, constraintValidatorContextMock);

        assertFalse(validationResult);
    }

    @Test
    public void isValid_WithValid7SignPassword_shouldReturnTrue(){
        String invalidPassword = generatePassword(7);
        PasswordValidator PasswordValidator = new PasswordValidator();

        boolean validationResult = PasswordValidator.isValid(invalidPassword, constraintValidatorContextMock);

        assertTrue(validationResult);
    }

    @Test
    public void isValid_WithValid35SignPassword_shouldReturnTrue(){
        String invalidPassword = generatePassword(35);
        PasswordValidator PasswordValidator = new PasswordValidator();

        boolean validationResult = PasswordValidator.isValid(invalidPassword, constraintValidatorContextMock);

        assertTrue(validationResult);
    }

}