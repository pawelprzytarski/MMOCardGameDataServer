package com.MmoCardGame.DataServer.shop.sellActions;

import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import com.MmoCardGame.DataServer.shop.products.ProductNames;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SellActionFactoryTest {


    @Test
    public void createSellAction_forMediumPacket_shouldCreateProperAction(){
        PacketSellAction sellAction = (PacketSellAction) SellActionFactory.produce(ProductNames.MEDIUM_PACKET);

        assertEquals(PacketsTypes.MEDIUM_PACKET, sellAction.getType());
    }

}