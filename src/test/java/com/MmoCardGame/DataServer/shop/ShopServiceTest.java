package com.MmoCardGame.DataServer.shop;


import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import com.MmoCardGame.DataServer.shop.products.Product;
import com.MmoCardGame.DataServer.shop.products.ProductCategories;
import com.MmoCardGame.DataServer.shop.products.ProductNames;
import com.MmoCardGame.DataServer.shop.productsFileManagement.ProductsLoader;
import com.MmoCardGame.DataServer.users.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class ShopServiceTest {

    private ProductsLoader productsLoader;
    private User user;

    @Before
    public void setup(){
        user = new User("login", "password", "email.email.com");
        user.setNumberOfCardsPacket(PacketsTypes.MEDIUM_PACKET, 0);
        user.setMoney(1000);

        List<Product> products = new ArrayList<>();
        products.add(new Product(ProductNames.MEDIUM_PACKET, 500, ProductCategories.PACKETS));

        productsLoader = Mockito.mock(ProductsLoader.class);
        Mockito.when(productsLoader.load()).thenReturn(products);

    }

    @Test
    public void buyPacket_byUserWithZeroPackets_shouldIncreaseHisPacketsAmount(){
        ShopService shopService = new ShopService(productsLoader);

        shopService.tryBuyProduct(user, ProductNames.MEDIUM_PACKET);

        assertThat(user.getNumberOfCardPackets().get(PacketsTypes.MEDIUM_PACKET)).isEqualTo(1);
    }

    @Test
    public void buyPacket_byUserWithZeroPackets_shouldDecreaseHisMoneyAmount(){
        ShopService shopService = new ShopService(productsLoader);

        shopService.tryBuyProduct(user, ProductNames.MEDIUM_PACKET);

        assertThat(user.getMoney()).isEqualTo(500);
    }

}