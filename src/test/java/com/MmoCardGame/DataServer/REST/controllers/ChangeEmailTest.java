package com.MmoCardGame.DataServer.REST.controllers;

import com.MmoCardGame.DataServer.REST.jsonMessages.ChangeEmailData;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.REST.utilConstants.ContentTypes;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.REST.responses.CredentialErrors;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ChangeEmailTest extends UtilControllerTest {

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changeEmail_withValidData_shouldReturnOk200Response() throws Exception{
        String newEmail = "newEmail@email.com";
        ChangeEmailData changeEmailData = new ChangeEmailData(newEmail,newEmail, existingRandomUser.getPassword());
        String changeDataJson = json(changeEmailData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/email")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changeEmail_withValidData_shouldChangeEmail() throws Exception{
        String newEmail = "newEmail@email.com";
        ChangeEmailData changeEmailData = new ChangeEmailData(newEmail,newEmail, existingRandomUser.getPassword());
        String changeDataJson = json(changeEmailData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/email")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson));

        User user = usersRepository.findByLogin(existingRandomUser.getLogin());
        assertEquals(newEmail, user.getEmail());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changeEmail_withWrongPassword_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        String newEmail = "newEmail@email.com";
        String invalidPassword = "invalidPass";
        ChangeEmailData changeEmailData = new ChangeEmailData(newEmail,newEmail, invalidPassword);
        String changeDataJson = json(changeEmailData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/email")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.WRONG_PASSWORD_MESSAGE));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changeEmail_withWrongPassword_shouldNotChangeEmail() throws Exception{
        String newEmail = "newEmail@email.com";
        String invalidPassword = "invalidPass";
        ChangeEmailData changeEmailData = new ChangeEmailData(newEmail,newEmail, invalidPassword);
        String changeDataJson = json(changeEmailData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/email")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson));

        User user = usersRepository.findByLogin(existingRandomUser.getLogin());
        assertEquals(user.getEmail(), existingRandomUser.getEmail());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changeEmail_withNotMatchingEmails_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        String newEmail = "newEmail@email.com";
        ChangeEmailData changeEmailData = new ChangeEmailData(newEmail,
                "a" + newEmail,
                existingRandomUser.getPassword());
        String changeDataJson = json(changeEmailData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/email")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.EMAILS_DONT_MATCH_MESSAGE));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changeEmail_withNotMatchingEmails_shouldNotChangeEmail() throws Exception{
        String newEmail = "newEmail@email.com";
        ChangeEmailData changeEmailData = new ChangeEmailData(newEmail,
                "a" + newEmail,
                existingRandomUser.getPassword());
        String changeDataJson = json(changeEmailData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/email")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson));

        User user = usersRepository.findByLogin(existingRandomUser.getLogin());
        assertEquals(user.getEmail(), existingRandomUser.getEmail());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changeEmail_withUsedEmail_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        ChangeEmailData changeEmailData = new ChangeEmailData(existingRandomUser.getEmail(),
                existingRandomUser.getEmail(),
                existingRandomUser.getPassword());
        String changeDataJson = json(changeEmailData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/email")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.EMAIL_USED_MESSAGE));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changeEmail_withUsedEmail_shouldNotChangeEmail() throws Exception{
        ChangeEmailData changeEmailData = new ChangeEmailData(existingRandomUser.getEmail(),
                existingRandomUser.getEmail(),
                existingRandomUser.getPassword());
        String changeDataJson = json(changeEmailData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/email")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson));

        User user = usersRepository.findByLogin(existingRandomUser.getLogin());
        assertEquals(user.getEmail(), existingRandomUser.getEmail());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changeEmail_withEmptyEmail_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        ChangeEmailData changeEmailData = new ChangeEmailData(null,
                existingRandomUser.getEmail(),
                existingRandomUser.getPassword());
        String changeDataJson = json(changeEmailData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/email")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$[0]").value(CredentialErrors.INVALID_EMAIL_MESSAGE));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changeEmail_withEmptyEmail_shouldNotChangeEmail() throws Exception{
        ChangeEmailData changeEmailData = new ChangeEmailData(null,
                existingRandomUser.getEmail(),
                existingRandomUser.getPassword());
        String changeDataJson = json(changeEmailData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/email")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson));

        User user = usersRepository.findByLogin(existingRandomUser.getLogin());
        assertEquals(user.getEmail(), existingRandomUser.getEmail());
    }

}