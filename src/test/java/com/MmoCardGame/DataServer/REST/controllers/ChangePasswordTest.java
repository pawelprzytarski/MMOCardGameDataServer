package com.MmoCardGame.DataServer.REST.controllers;

import com.MmoCardGame.DataServer.REST.jsonMessages.ChangePasswordData;
import com.MmoCardGame.DataServer.REST.responses.CredentialErrors;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.REST.utilConstants.ContentTypes;
import com.MmoCardGame.DataServer.users.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class ChangePasswordTest extends UtilControllerTest {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changePassword_withValidData_shouldReturnOk200Response() throws Exception{
        String newPassword = "newPassword";
        ChangePasswordData changePasswordData =
                new ChangePasswordData(
                newPassword,
                newPassword,
                existingRandomUser.getPassword());
        String changeDataJson = json(changePasswordData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/password")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changePassword_withValidData_shouldChangePassword() throws Exception{
        String newPassword = "newPassword";
        ChangePasswordData changePasswordData =
                new ChangePasswordData(
                        newPassword,
                        newPassword,
                        existingRandomUser.getPassword());
        String changeDataJson = json(changePasswordData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/password")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson));

        User user = usersRepository.findByLogin(existingRandomUser.getLogin());
        boolean result = passwordEncoder.matches(newPassword, user.getPassword());
        assertTrue(result);
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changePassword_withWrongOldPassword_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        String newPassword = "newPassword";
        String wrongOldPassword = existingRandomUser.getPassword() + "a";
        ChangePasswordData changePasswordData =
                new ChangePasswordData(
                        newPassword,
                        newPassword,
                        wrongOldPassword);
        String changeDataJson = json(changePasswordData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/password")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.WRONG_PASSWORD_MESSAGE));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changePassword_withWrongOldPassword_shouldNotChangePassword() throws Exception{
        String newPassword = "newPassword";
        String wrongOldPassword = existingRandomUser.getPassword() + "a";
        ChangePasswordData changePasswordData =
                new ChangePasswordData(
                        newPassword,
                        newPassword,
                        wrongOldPassword);
        String changeDataJson = json(changePasswordData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/password")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson));

        User user = usersRepository.findByLogin(existingRandomUser.getLogin());
        assertEquals(existingRandomUser.getPassword(), user.getPassword());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changePassword_withNotMatchingPasswords_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        String newPassword = "newPassword";
        String notMatchingNewPassword = newPassword + "a";
        ChangePasswordData changePasswordData =
                new ChangePasswordData(
                        newPassword,
                        notMatchingNewPassword,
                        existingRandomUser.getPassword());
        String changeDataJson = json(changePasswordData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/password")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.PASSWORDS_DONT_MATCH_MESSAGE));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changePassword_withNotMatchingPasswords_shouldNotChangePassword() throws Exception{
        String newPassword = "newPassword";
        String notMatchingNewPassword = newPassword + "a";
        ChangePasswordData changePasswordData =
                new ChangePasswordData(
                        newPassword,
                        notMatchingNewPassword,
                        existingRandomUser.getPassword());
        String changeDataJson = json(changePasswordData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/password")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson));

        User user = usersRepository.findByLogin(existingRandomUser.getLogin());
        assertEquals(existingRandomUser.getPassword(), user.getPassword());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changePassword_withEmptyPassword_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        String newPassword = "newPassword";
        ChangePasswordData changePasswordData =
                new ChangePasswordData(
                        null,
                        newPassword,
                        existingRandomUser.getPassword());
        String changeDataJson = json(changePasswordData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/password")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$[0]").value(CredentialErrors.INVALID_PASSWORD_MESSAGE));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changePassword_withEmptyPassword_shouldNotChangePassword() throws Exception{
        String newPassword = "newPassword";
        ChangePasswordData changePasswordData =
                new ChangePasswordData(
                        null,
                        newPassword,
                        existingRandomUser.getPassword());
        String changeDataJson = json(changePasswordData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/password")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isBadRequest());

        User user = usersRepository.findByLogin(existingRandomUser.getLogin());
        assertEquals(existingRandomUser.getPassword(), user.getPassword());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void changePassword_withInvalidNewPassword_shouldReturnBadRequest400ResponseAndMessage() throws Exception{
        String invalidNewPassword = "new";
        ChangePasswordData changePasswordData =
                new ChangePasswordData(
                        invalidNewPassword,
                        invalidNewPassword,
                        existingRandomUser.getPassword());
        String changeDataJson = json(changePasswordData);

        mockMvc.perform(put(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/password")
                .contentType(ContentTypes.JSON_UTF8)
                .content(changeDataJson))
                .andExpect(status().isBadRequest())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$[0]").value(CredentialErrors.INVALID_PASSWORD_MESSAGE));
    }

}