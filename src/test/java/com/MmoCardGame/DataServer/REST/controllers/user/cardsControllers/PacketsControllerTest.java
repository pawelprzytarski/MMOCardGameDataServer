package com.MmoCardGame.DataServer.REST.controllers.user.cardsControllers;

import com.MmoCardGame.DataServer.REST.controllers.UtilControllerTest;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.REST.utilConstants.ContentTypes;
import com.MmoCardGame.DataServer.cards.packets.CardsPacketOpener;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsRepository;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsService;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import com.MmoCardGame.DataServer.cards.packets.PacketsTypes;
import com.MmoCardGame.DataServer.users.UsersRepository;
import com.MmoCardGame.DataServer.users.UsersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(PacketsController.class)
@ContextConfiguration
public class PacketsControllerTest extends UtilControllerTest {

    @Configuration
    static class CardsCollectionTestContextConfiguration {
        @Bean
        public CardsPacketOpener cardPacketOpener(){return new CardsPacketOpener(cardsService());}
        @Bean
        public CardsService cardsService() {
            return new CardsService(cardsRepository());
        }
        @Bean
        public CardsRepository cardsRepository() {
            return Mockito.mock(CardsRepository.class);
        }
        @Bean
        public UsersService usersService() {
            return new UsersService();
        }
        @Bean
        public UsersRepository usersRepository() {
            return Mockito.mock(UsersRepository.class);
        }
        @Bean
        public CardsCollectionController cardsCollectionController(){
            return new CardsCollectionController(usersService());
        }
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PacketsController packetsController;

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void openPacket_withOneUserPacket_shouldReturnOk200Response() throws Exception{
        Card card = new Card();

        Mockito
                .when(packetsController.openPacket(existingRandomUser.getLogin(), PacketsTypes.MEDIUM_PACKET))
                .thenReturn(UtilResponsesBuilder.buildOkResponseWithBody(card));

        mockMvc.perform(get(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/packets/" + PacketsTypes.MEDIUM_PACKET))
                .andExpect(status().isOk())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8));
    }
}