package com.MmoCardGame.DataServer.REST.controllers;

import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.REST.utilConstants.ContentTypes;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersRepository;
import com.MmoCardGame.DataServer.REST.responses.CredentialErrors;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.mock.http.MockHttpOutputMessage;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class UsersControllerTest {

    private MockMvc mockMvc;

    private List<User> users = new ArrayList<>();

    @Autowired
    private UsersController controller;

    @Autowired
    private UsersRepository usersRepositoryMock;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private HttpMessageConverter mappingJackson2HttpMessageConverter;

    @Autowired
    void setConverters(HttpMessageConverter<?>[] converters) {
        this.mappingJackson2HttpMessageConverter = Arrays.asList(converters).stream()
                .filter(hmc -> hmc instanceof MappingJackson2HttpMessageConverter)
                .findAny()
                .orElse(null);

        assertNotNull("the JSON message converter must not be null",
                this.mappingJackson2HttpMessageConverter);
    }

    @Before
    public void setup(){
        this.mockMvc = webAppContextSetup(webApplicationContext).build();

        usersRepositoryMock.deleteAll();

        User user1 = new User("qwe", "qwe", "qwe@qwe.qwe");
        usersRepositoryMock.save(user1);
        users.add(user1);
        User user2 = new User("asd", "asd", "asd@asd.asd");
        usersRepositoryMock.save(user2);
        users.add(user2);
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void getUsers_shouldReturnOk200Response() throws Exception{
        mockMvc.perform(get(ApiPaths.ADMIN_USERS_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "ADMIN")
    public void getUsers_shouldReturnUsersJSON() throws Exception{
        mockMvc.perform(get(ApiPaths.ADMIN_USERS_URL))
                .andExpect(status().isOk())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].id", is(users.get(0).getId().intValue())))
                .andExpect(jsonPath("$[0].login", is("qwe")))
                .andExpect(jsonPath("$[0].password", is("qwe")))
                .andExpect(jsonPath("$[1].id", is(users.get(1).getId().intValue())))
                .andExpect(jsonPath("$[1].login", is("asd")))
                .andExpect(jsonPath("$[1].password", is("asd")));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void getNotExistingUser_shouldReturnNotFound404Response() throws Exception{
        mockMvc.perform(get(ApiPaths.ADMIN_USERS_URL + "/-1"))
                .andExpect(status().isNotFound());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void getUser_shouldReturnOk200Response() throws Exception{
        mockMvc.perform(get(ApiPaths.ADMIN_USERS_URL + "/" + users.get(0).getId()))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void getUser_shouldReturnProperUserJSON() throws Exception{
        mockMvc.perform(get(ApiPaths.ADMIN_USERS_URL + "/" + users.get(0).getId()))
                .andExpect(status().isOk())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$.id", is(users.get(0).getId().intValue())))
                .andExpect(jsonPath("$.login", is("qwe")))
                .andExpect(jsonPath("$.password", is("qwe")));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void addUser_shouldReturnCreated201Response() throws Exception{
        User newUser = new User("zxc", "zxc", "zxc@zxc.zxc");
        String newUserJson = json(newUser);
        mockMvc.perform(post(ApiPaths.ADMIN_USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(newUserJson))
                .andExpect(status().isCreated());
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void addExistingUser_shouldReturnConflict409ResponseAndMessage() throws Exception{
        User newUser = new User("qwe", "qwe", "qdsafwe@qwe.qwe");
        String newUserJson = json(newUser);
        mockMvc.perform(post(ApiPaths.ADMIN_USERS_URL)
                .contentType(ContentTypes.JSON_UTF8)
                .content(newUserJson))
                .andExpect(status().isConflict())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$").value(CredentialErrors.LOGIN_USED_MESSAGE));
    }

    protected String json(Object o) throws IOException {
        MockHttpOutputMessage mockHttpOutputMessage = new MockHttpOutputMessage();
        this.mappingJackson2HttpMessageConverter.write(
                o, ContentTypes.JSON_UTF8, mockHttpOutputMessage);
        return mockHttpOutputMessage.getBodyAsString();
    }

}
