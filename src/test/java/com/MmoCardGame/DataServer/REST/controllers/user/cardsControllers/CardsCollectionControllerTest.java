package com.MmoCardGame.DataServer.REST.controllers.user.cardsControllers;

import com.MmoCardGame.DataServer.REST.jsonMessages.CardWithAmount;
import com.MmoCardGame.DataServer.REST.responses.UtilResponsesBuilder;
import com.MmoCardGame.DataServer.REST.utilConstants.ApiPaths;
import com.MmoCardGame.DataServer.REST.utilConstants.ContentTypes;
import com.MmoCardGame.DataServer.cards.CardTypes;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsRepository;
import com.MmoCardGame.DataServer.cards.cardsCollection.CardsService;
import com.MmoCardGame.DataServer.cards.cardsEntities.Card;
import com.MmoCardGame.DataServer.cards.packets.CardsPacketOpener;
import com.MmoCardGame.DataServer.users.User;
import com.MmoCardGame.DataServer.users.UsersRepository;
import com.MmoCardGame.DataServer.users.UsersService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Collections;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(CardsCollectionController.class)
@ContextConfiguration
public class CardsCollectionControllerTest {

    @Configuration
    static class CardsCollectionTestContextConfiguration {
        @Bean
        public CardsPacketOpener cardPacketOpener(){return new CardsPacketOpener(cardsService());}
        @Bean
        public CardsService cardsService() {
            return new CardsService(cardsRepository());
        }
        @Bean
        public CardsRepository cardsRepository() {
            return Mockito.mock(CardsRepository.class);
        }
        @Bean
        public UsersService usersService() {
            return new UsersService();
        }
        @Bean
        public UsersRepository usersRepository() {
            return Mockito.mock(UsersRepository.class);
        }
        @Bean
        public CardsCollectionController cardsCollectionController(){
            return new CardsCollectionController(usersService());
        }
    }

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CardsCollectionController cardsCollectionController;

    private CardWithAmount cardWithAmount;

    private User existingRandomUser = new User("randomLogin", "randomPassword", "email@email.com");

    @Before
    public void setupCards(){
        Card randomCard = new Card(CardTypes.OTHER);
        randomCard.setId(1L);

        cardWithAmount = new CardWithAmount();
        cardWithAmount.setAmount(1);
        cardWithAmount.setCardId(randomCard.getId());

        Mockito
                .when(cardsCollectionController.getAllUserCards(existingRandomUser.getLogin()))
                .thenReturn(UtilResponsesBuilder.buildOkResponseWithBody(Collections.singletonList(cardWithAmount)));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void getCardsCollection_shouldReturnOk200Response() throws Exception{

        mockMvc.perform(get(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/cards"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8));
    }

    @Test
    @WithMockUser(username = "test", password = "test", roles = "USER")
    public void getCardsCollection_whileUserHaveOneCard_shouldReturnCardsCollectionWithThisCard() throws Exception{

        mockMvc.perform(get(ApiPaths.USERS_URL + "/" + existingRandomUser.getLogin() + "/cards"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(ContentTypes.JSON_UTF8))
                .andExpect(jsonPath("$[0].cardId", is(((int) cardWithAmount.getCardId()))))
                .andExpect(jsonPath("$[0].amount", is(cardWithAmount.getAmount())));

    }

}